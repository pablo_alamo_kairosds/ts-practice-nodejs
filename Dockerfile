FROM node:16-alpine3.14 AS builder
RUN mkdir /builder
WORKDIR /builder
COPY . .
RUN npm install
RUN npm run build
RUN npm prune --production

FROM node:16-alpine3.14 AS production
WORKDIR /usr/src/app
COPY --from=builder /builder/package.json ./package.json
COPY --from=builder /builder/package-lock.json ./package-lock.json
COPY --from=builder /builder/node_modules ./node_modules
COPY --from=builder /builder/dist ./dist

RUN npm install

EXPOSE 3000
CMD [ "npm", "run", "start:prod" ]
