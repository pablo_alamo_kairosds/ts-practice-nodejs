<h1 align="center">TS Express and Mongo Practice</h1>

<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#built-with">Built With</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#installation">Installation</a></li>
        <li><a href="#run-application">Run Application</a></li>
        <li><a href="#lint">Lint</a></li>
        <li><a href="#running-tests">Running Tests</a></li>
        <li><a href="#build">Build</a></li>
      </ul>
    </li>
    <li><a href="#rest-api">REST API</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

## Built With

* [Typescript](https://www.typescriptlang.org/) (v4.7.4)
* [Express](https://expressjs.com/)
* [Docker](https://www.docker.com/)

## Getting Started

It can't be more simple to install the project.

### Installation

1. Clone the repo in your directory
   ```sh
   git clone https://gitlab.com/pablo_alamo_kairosds/ts-practice-nodejs.git .
   ```
2. Install NPM packages
   ```sh
   npm i
   ```

### Run Application

#### Option 1: Local

1. Create a .env file based on the .env.example
  ```sh
  cp .env.example .env
  ```

2. Run the mongo database, you can run it locally or use simply this command (by default on port 27017):
  ```sh
  docker-compose up -d
  ```

3. Run server in local with nodemon (by default on port 3000)
  ```sh
  npm start
  ```

#### Option 2: Docker

> Run server and db in development mode
  ```sh
  npm run up
  ```

> Run server and db in production mode
  ```sh
  npm run up:prod
  ```

### Lint 

```sh
npm run lint
```
  
### Running Tests

```sh
npm test
```
  
### Build

```sh
npm run build
```

## REST API

The REST API to the example app is described below with examples.

`[ Base URL: localhost:3000/api/v1 ]`

### Create a new Offensive Word
***
#### Request

`POST /offensive-word/`
```sh
curl -i -X POST http://localhost:3000/api/v1/offensive-word \
     -H "Content-Type: application/json" \
     -d '{"word": "noob", "level": 3}'
```

#### Response

    {
      "id": "c7b169c3-a1e6-4508-b029-fce4f1f25a8b",
      "word": "noob",
      "level": 3
    }

### Get list of Offensive Words
***
#### Request

`GET /offensive-word/`
```sh
curl -i http://localhost:3000/api/v1/offensive-word
```

#### Response

    [
      {
        "id": "c7b169c3-a1e6-4508-b029-fce4f1f25a8b",
        "word": "noob",
        "level": 3
      },
      {
        "id": "8739e332-47a4-4cd2-b411-d322c5f5e80b",
        "word": "troll",
        "level": 2
      }
    ]

### Get a specific Offensive Word
***
#### Request

`GET /offensive-word/id`
```sh
curl -i -X POST http://localhost:3000/api/v1/offensive-word/8739e332-47a4-4cd2-b411-d322c5f5e80b
```

#### Response

    {
      "id": "8739e332-47a4-4cd2-b411-d322c5f5e80b",
      "word": "troll",
      "level": 2
    }

### Update a specific Offensive Word
***
#### Request

`PUT /offensive-word/uuid`
```sh
curl -i -X PUT http://localhost:3000/api/v1/offensive-word/8739e332-47a4-4cd2-b411-d322c5f5e80b \
     -H "Content-Type: application/json" \
     -d '{"word": "noobie", "level": 1}'
```

#### Response

    {
      "id": "8739e332-47a4-4cd2-b411-d322c5f5e80b",
      "word": "noobie",
      "level": 1
    }

### Delete an Offensive Word
***
#### Request

`DELETE /offensive-word/uuid`
```sh
curl -i -X DELETE http://localhost:3000/api/v1/offensive-word/8739e332-47a4-4cd2-b411-d322c5f5e80b
```

#### Response

    {
      "id": "8739e332-47a4-4cd2-b411-d322c5f5e80b",
      "word": "noobie",
      "level": 1
    }

## Contact

Pablo Álamo - [LinkedIn](https://www.linkedin.com/in/pablodevs/) - pablo.alamo@kairosds.com

Project Link: [https://gitlab.com/pablo_alamo_kairosds/ts-practice-nodejs](https://gitlab.com/pablo_alamo_kairosds/ts-practice-nodejs)

<p align="right">(<a href="#top">back to top</a>)</p>
