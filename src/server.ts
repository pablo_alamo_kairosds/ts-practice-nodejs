import app from './app';
import { createAssosationsPG } from './infrastructure/config/associations';
import { client } from './infrastructure/config/redis';
import { connect as connectToMongo } from './infrastructure/config/mongo';
import { populateDatabases } from './infrastructure/config/populate';
import sequelize, { init as connectPG } from './infrastructure/config/postgresql';

(async () => {
  await connectPG();

  await connectToMongo();
  console.log('Mongoose connected to MongoDB');
  
  createAssosationsPG();
  
  await client.connect();
})();

const port = process.env.APP_PORT ?? 3000;

app.listen(port, async () => {
  console.log(`Server started on port ${ port }`);

  sequelize.sync({ force: true })
    .then(() => {
      console.log('Database & tables created');
      populateDatabases();
    });
});
