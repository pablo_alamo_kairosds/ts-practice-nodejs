import 'reflect-metadata';

import { config } from 'dotenv';
config();

import cors from 'cors';
import express, { Application, Request, Response } from 'express';
import expressPinoLogger from 'express-pino-logger';
import passport from 'passport';
import path from 'path';
import swaggerUI from 'swagger-ui-express';
import Container from 'typedi';
import YAML from 'yamljs';
import { logger } from './infrastructure/config/logger';
import { contextMiddleware } from './infrastructure/middlewares/context';
import { errorMiddleware } from './infrastructure/middlewares/errors';
import passportMiddleware from './infrastructure/middlewares/passport';
import { OffensiveWordRepositoryMongo } from './infrastructure/repositories/offensive-word.repository.mongo';
import { PostRepositoryPG } from './infrastructure/repositories/post.repository.pg';
import { UserRepositoryPG } from './infrastructure/repositories/user.repository.pg';
import { OffensiveWordRouter } from './infrastructure/routes/offensive-word.route';
import { PostRouter } from './infrastructure/routes/post.route';
import { AuthRouter } from './infrastructure/routes/user.route';
import { client } from './infrastructure/config/redis';

Container.set('OffensiveWordRepository', new OffensiveWordRepositoryMongo());
Container.set('UserRepository', new UserRepositoryPG());
Container.set('PostRepository', new PostRepositoryPG());

const baseEndpoint = '/api/v1';

const app: Application = express();
app.use(cors());
app.use(express.json());
app.use(contextMiddleware);
app.use(expressPinoLogger(logger));
app.use(passport.initialize());
passport.use(passportMiddleware);

const swaggerDocument = YAML.load(path.join(__dirname, './open-api.yaml'));
app.use(`${ baseEndpoint }/api-docs`, swaggerUI.serve, swaggerUI.setup(swaggerDocument));

// Redis
client.on('error', (err: any) => console.log('Redis Client Error', err));

// Offensive words routes
app.use(`${ baseEndpoint }/offensive-word`, OffensiveWordRouter);
// Auth routes
app.use(`${ baseEndpoint }/auth`, AuthRouter);
// Posts routes
app.use(`${ baseEndpoint }/post`, PostRouter);

// Healthcheck
app.get(`${ baseEndpoint }/status`, (req: Request, res: Response) => {
  return res.sendStatus(200);
});

app.use(errorMiddleware);

export default app;