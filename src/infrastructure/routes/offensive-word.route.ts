import express from 'express';
import { body, query } from 'express-validator';
import passport from 'passport';
import { Role } from '../../domain/model/vos/role.vo';
import { createOffensiveWordController } from '../controllers/offensive-word/create-offensive-word.controller';
import { deleteManyOffensiveWordController } from '../controllers/offensive-word/delete-many-offensive-words.controller';
import { deleteOffensiveWordController } from '../controllers/offensive-word/delete-offensive-word.controller';
import { getAllOffensiveWordsController } from '../controllers/offensive-word/get-all-offensive-words.controller';
import { updateOffensiveWordController } from '../controllers/offensive-word/update-offensive-word.controller';
import { hasRole } from '../middlewares/role';
import { validateReqFormat } from '../middlewares/validator';

const router = express.Router();

router.get('/',
  passport.authenticate('jwt', { session: false }),
  hasRole([Role.ADMIN]),
  getAllOffensiveWordsController()
);

router.post('/',
  passport.authenticate('jwt', { session: false }),
  hasRole([Role.ADMIN]),
  body('word', 'Please enter a valid word').notEmpty().isString().trim(),
  body('level', 'Please enter a numeric type level').notEmpty().custom(value => typeof value === 'number'),
  validateReqFormat(),
  createOffensiveWordController()
);

router.put('/:id',
  passport.authenticate('jwt', { session: false }),
  hasRole([Role.ADMIN]),
  body('word', 'Please enter a valid word').notEmpty().isString().trim(),
  body('level', 'Please enter a numeric type level').notEmpty().custom(value => typeof value === 'number'),
  validateReqFormat(),
  updateOffensiveWordController()
);

router.delete('/:id',
  passport.authenticate('jwt', { session: false }),
  hasRole([Role.ADMIN]),
  deleteOffensiveWordController()
);

router.delete('/',
  passport.authenticate('jwt', { session: false }),
  hasRole([Role.ADMIN]),
  query('ids').notEmpty().isString(),
  validateReqFormat(),
  deleteManyOffensiveWordController()
);

export { router as OffensiveWordRouter };
