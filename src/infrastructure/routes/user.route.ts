import express from 'express';
import { body } from 'express-validator';
import passport from 'passport';
import { updateCache } from '../middlewares/cache';
import { deleteUserController } from '../controllers/user/delete-user.controller';
import { getUserDataController } from '../controllers/user/get-user-data.controller';
import { loginController } from '../controllers/user/login.controller';
import { signUpController } from '../controllers/user/sign-up.controller';
import { hasPermission, PermissionOption } from '../middlewares/permission';
import { validateReqFormat } from '../middlewares/validator';

const router = express.Router();

router.post(
  '/login',
  body('email').notEmpty().isEmail(),
  body('password').notEmpty().isString(),
  validateReqFormat(),
  loginController()
);

router.post(
  '/sign-up',
  body('email').notEmpty().isEmail(),
  body('password').notEmpty().isString(),
  body('role').notEmpty().isIn(['USER', 'AUTHOR']),
  validateReqFormat(),
  signUpController()
);

router.delete('/:userId',
  passport.authenticate('jwt', { session: false }),
  hasPermission(PermissionOption.USER),
  updateCache(),
  deleteUserController()
);

router.get('/data/me',
  passport.authenticate('jwt', { session: false }),
  getUserDataController()
);

export { router as AuthRouter };
