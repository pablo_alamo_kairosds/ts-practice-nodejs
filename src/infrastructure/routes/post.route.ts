import express from 'express';
import { body } from 'express-validator';
import passport from 'passport';
import { Role } from '../../domain/model/vos/role.vo';
import { createCommentController } from '../controllers/post/create-comment.controller';
import { createPostController } from '../controllers/post/create-post.controller';
import { deleteCommentController } from '../controllers/post/delete-comment.controller';
import { deletePostController } from '../controllers/post/delete-post.controller';
import { getAllPostsController } from '../controllers/post/get-all-posts.controller';
import { getSinglePostController } from '../controllers/post/get-single-post.controller';
import { updateCommentController } from '../controllers/post/update-comment.controller';
import { updatePostController } from '../controllers/post/update-post.controller';
import { cache, updateCache } from '../middlewares/cache';
import { hasPermission, PermissionOption } from '../middlewares/permission';
import { hasRole } from '../middlewares/role';
import { validateReqFormat } from '../middlewares/validator';

const router = express.Router();

// Recuperación de todas las entradas(sin comentarios)
router.get('/', cache(), getAllPostsController());

// Recuperación de una entrada(con comentarios)
router.get('/:postId', cache(), getSinglePostController());

// Creación de una nueva entrada(sin comentarios)
router.post('/',
  passport.authenticate('jwt', { session: false }),
  hasRole([Role.AUTHOR, Role.ADMIN]),
  body('title').notEmpty().isString(),
  body('content').notEmpty().isString(),
  validateReqFormat(),
  updateCache(),
  createPostController()
);

// Borrado de una entrada existente (con todos sus comentarios)
router.delete('/:postId',
  passport.authenticate('jwt', { session: false }),
  hasRole([Role.AUTHOR, Role.ADMIN]),
  hasPermission(PermissionOption.POST),
  updateCache(),
  deletePostController()
);

// Modificación de un post existente (se actualizará todo excepto comentarios)
router.put('/:postId',
  passport.authenticate('jwt', { session: false }),
  hasRole([Role.AUTHOR, Role.ADMIN]),
  hasPermission(PermissionOption.POST),
  body('title').notEmpty().isString(),
  body('content').notEmpty().isString(),
  validateReqFormat(),
  updateCache(),
  updatePostController()
);

// Adición de un nuevo comentario a una entrada
router.post('/:postId/comment',
  passport.authenticate('jwt', { session: false }),
  body('message').notEmpty().isString(),
  validateReqFormat(),
  updateCache(),
  createCommentController()
);

// Modificación de un comentario existente
router.put('/:postId/comment/:commentId',
  passport.authenticate('jwt', { session: false }),
  hasPermission(PermissionOption.COMMENT),
  body('message').notEmpty().isString(),
  validateReqFormat(),
  updateCache(),
  updateCommentController()
);

// Borrado de un comentario existente en una entrada
router.delete('/:postId/comment/:commentId',
  passport.authenticate('jwt', { session: false }),
  hasPermission(PermissionOption.COMMENT),
  updateCache(),
  deleteCommentController()
);

export { router as PostRouter };
