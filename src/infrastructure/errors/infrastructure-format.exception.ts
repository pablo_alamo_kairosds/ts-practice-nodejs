import { InfrastructureException } from './infrastructure.exception';

export class InfrastructureFormatException extends InfrastructureException {

  constructor(public message: string) {
    super(message);
  }

}