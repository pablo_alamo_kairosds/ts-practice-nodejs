import { CustomError } from 'ts-custom-error';

export class InfrastructureException extends CustomError { }