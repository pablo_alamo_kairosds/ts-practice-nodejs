import { JwtPayload } from 'jsonwebtoken';
import { ExtractJwt, Strategy, StrategyOptions } from 'passport-jwt';
import { Container } from 'typedi';
import { User } from '../../domain/model/entities/user.entity';
import { EmailVO } from '../../domain/model/vos/email.vo';
import { UserService } from '../../domain/services/user.service';

const opts: StrategyOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.SECRET ?? 'secret'
};

export default new Strategy(opts, async (payload: JwtPayload, done) => {
  // PAYLOAD example:
  // {
  //   "email": "pablo@admin.com",
  //   "iat": 1657804667,
  //   "exp": 1657891067
  // }
  try {

    const email: string = payload.email;
    const userService = Container.get(UserService);
    const user: User | null = await userService.getByEmail(EmailVO.create(email));
    
    if (user) {
      return done(null, user);
    }

    return done(null, false, {message: 'User not Found'});

  } catch (err) {
    console.error(err);
  }

});