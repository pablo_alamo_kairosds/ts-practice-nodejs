import { NextFunction, Request, Response } from 'express';
import Container from 'typedi';
import { DomainForbiddenException } from '../../domain/errors/domain-forbidden.exception';
import { Comment } from '../../domain/model/entities/comment.entity';
import { Post } from '../../domain/model/entities/post.entity';
import { User } from '../../domain/model/entities/user.entity';
import { IdVO } from '../../domain/model/vos/id.vo';
import { Role } from '../../domain/model/vos/role.vo';
import { PostService } from '../../domain/services/post.service';

export enum PermissionOption {
  POST = 'POST',
  COMMENT = 'COMMENT',
  USER = 'USER'
}

export const hasPermission = (option: PermissionOption) => {

  return async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const loggedUser = req.user as User;
    const postService = Container.get(PostService);

    try {

      switch (option) {
        case PermissionOption.POST: {
          const postId = req.params.postId;
          const post: Post = await postService.getPostById(IdVO.createWithUUID(postId));
          if (post.userId !== loggedUser.id && loggedUser.role !== Role.ADMIN) {
            throw new DomainForbiddenException();
          }
          break;
        }

        case PermissionOption.COMMENT: {
          const commentId = req.params.commentId;
          const comment: Comment = await postService.getCommentById(IdVO.createWithUUID(commentId));
          if (comment.userId !== loggedUser.id && loggedUser.role !== Role.ADMIN) {
            throw new DomainForbiddenException();
          }
          break;
        }

        case PermissionOption.USER: {
          const userToBeDeletedId = IdVO.createWithUUID(req.params.userId).value;
          if (userToBeDeletedId !== loggedUser.id && loggedUser.role !== Role.ADMIN) {
            throw new DomainForbiddenException();
          }
          break;
        }

      }

    } catch (err) {
      next(err);
      return;
    }

    next();
    return;
  };

};