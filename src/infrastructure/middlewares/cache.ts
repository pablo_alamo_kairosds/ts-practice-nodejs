import { NextFunction, Request, Response } from 'express';
import Container from 'typedi';
import { GetAllPostsUseCase } from '../../applications/usecases/post/get-all-posts.usecase';
import { client } from '../config/redis';

const EXPIRATION_TIME = 10;

export const cache = () => {
  return async (req: Request, res: Response, next: NextFunction): Promise<Response | void> => {
    try {
      const key = '__cache__' + req.originalUrl;
      const data: string | null = await client.get(key);

      if (data) {
        return res.status(200).json(JSON.parse(data));
      }

      res.on('finish', async () => {
        const { data } = res.locals;

        if (data) {
          await client.set(key, JSON.stringify(data), { EX: EXPIRATION_TIME });
        }

      });

      next();
    } catch (err) {
      next(err);
    }
  };
};

export const updateCache = () => {
  return async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      res.on('finish', async () => {
        await client.flushAll();

        const getAllPostsUseCase = Container.get(GetAllPostsUseCase);
        const posts = await getAllPostsUseCase.execute();

        await client.set('/api/v1/post/', JSON.stringify(posts), { EX: EXPIRATION_TIME });
      });

      next();
    } catch (err) {
      next(err);
    }
  };
};