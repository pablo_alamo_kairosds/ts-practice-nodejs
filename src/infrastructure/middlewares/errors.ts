import { NextFunction, Request, Response } from 'express';
import { DomainConflictException } from '../../domain/errors/domain-conflict.exception';
import { DomainForbiddenException } from '../../domain/errors/domain-forbidden.exception';
import { DomainFormatException } from '../../domain/errors/domain-format.exception';
import { DomainNotFoundException } from '../../domain/errors/domain-not-found.exception';
import { DomainUnauthorizedException } from '../../domain/errors/domain-unauthorized.exception';
import { logger } from '../config/logger';
import { InfrastructureFormatException } from '../errors/infrastructure-format.exception';

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export const errorMiddleware = (error: Error, req: Request, res: Response, next: NextFunction): Response => {

  logger.error(error);
  
  if (error instanceof DomainFormatException) {
    return res.status(400).json({ error: error.message });
  }

  if (error instanceof DomainNotFoundException) {
    return res.status(404).json({ error: error.message });
  }

  if (error instanceof DomainConflictException) {
    return res.status(409).json({ error: error.message });
  }

  if (error instanceof DomainUnauthorizedException) {
    return res.status(401).json({ error: error.message });
  }

  if (error instanceof DomainForbiddenException) {
    return res.status(403).json({ error: error.message });
  }

  if (error instanceof InfrastructureFormatException) {
    const messages = JSON.parse(error.message);
    if (messages.length > 1) {
      return res.status(400).json({ errors: messages });
    }
    return res.status(400).json({ error: messages[0] });
  }

  return res.status(500).json({ error: 'Internal server error' });
};