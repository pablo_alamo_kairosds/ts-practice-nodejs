import { NextFunction, Request, Response } from 'express';
import { ValidationError, validationResult } from 'express-validator';
import { InfrastructureFormatException } from '../errors/infrastructure-format.exception';

export const validateReqFormat = () => {

  return (req: Request, res: Response, next: NextFunction): void => {

    try {
      const errorFormatter = ({ location, msg, param, value }: ValidationError) =>
        `Received: '${ value }'. ${ msg } in ${ location }[${ param }]`;
        
      const result = validationResult(req).formatWith(errorFormatter);

      if (!result.isEmpty()) {
        throw new InfrastructureFormatException(JSON.stringify(result.array()));
      }

    } catch (err) {
      next(err);
      return;
    }

    next();
  };

};