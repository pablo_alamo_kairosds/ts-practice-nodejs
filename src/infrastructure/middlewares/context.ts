import { Request, Response } from 'express';
import { IdVO } from '../../domain/model/vos/id.vo';
import { context } from '../config/async-context';

export const contextMiddleware = (req: Request, res: Response, next: any) => {
  const id = req.get('x-correlation-id') || IdVO.create().value;
  context.run(id, next);
};