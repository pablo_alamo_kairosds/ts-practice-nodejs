import { NextFunction, Request, Response } from 'express';
import { User } from '../../domain/model/entities/user.entity';
import { Role } from '../../domain/model/vos/role.vo';

export const hasRole = (roles: Role[]) => {

  return (req: Request, res: Response, next: NextFunction): Response | void => {

    const user = req.user as User;
    const userRole = user.role;

    const allow = roles.some(role => userRole === role);

    if (allow) {
      next();
      return;
    }
    return res.status(403).json({ error: 'Not allowed' });
  };

};