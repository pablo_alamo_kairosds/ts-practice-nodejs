import { DataTypes } from 'sequelize';
import sequelize from '../config/postgresql';

export const CommentModel = sequelize.define('comments', {

  id: {
    type: DataTypes.UUID,
    allowNull: false,
    primaryKey: true
  },
  message: {
    type: DataTypes.STRING,
    allowNull: false
  }
  
});