import { Comment } from '../../domain/model/entities/comment.entity';
import { Post } from '../../domain/model/entities/post.entity';
import { ContentVO } from '../../domain/model/vos/content.vo';
import { IdVO } from '../../domain/model/vos/id.vo';
import { MessageVO } from '../../domain/model/vos/message.vo';
import { TitleVO } from '../../domain/model/vos/title.vo';
import { PostRepository } from '../../domain/repositories/post.repository';
import { CommentModel } from './comment.schema';
import { PostModel } from './post.schema';
import { UserModel } from './user.schema';

export class PostRepositoryPG implements PostRepository {

  async savePost(post: Post): Promise<void> {
    const id: string = post.id;
    const title: string = post.title;
    const content: string = post.content;
    const userId: string = post.userId;

    const userDB: any = await UserModel.findOne({ where: { id: userId } });
    await userDB.createPost({ id, title, content });
  }

  async deletePost(postIdVO: IdVO): Promise<void> {
    await PostModel.destroy({ where: { id: postIdVO.value } });
  }

  async updatePost(post: Post): Promise<void> {
    const postDB: any = await PostModel.findOne({ where: { id: post.id } });
    await postDB.update({
      title: post.title,
      content: post.content
    });
  }

  async getAllPosts(): Promise<Post[]> {
    const postsDB: any[] = await PostModel.findAll();

    const posts = Promise.all(postsDB.map(async (postDB) => {
      const comments: Comment[] = await this.getPostComments(postDB);
      return this.mapPostDBToEntity(postDB, comments);
    }));

    return posts;
  }

  async getPostById(postIdVO: IdVO): Promise<Post | null> {
    const postDB: any = await PostModel.findOne({ where: { id: postIdVO.value } });

    if (!postDB) {
      return null;
    }

    const comments: Comment[] = await this.getPostComments(postDB);
    return this.mapPostDBToEntity(postDB, comments);
  }

  async getCommentById(commentIdVO: IdVO): Promise<Comment | null> {
    const commentDB: any = await CommentModel.findOne({ where: { id: commentIdVO.value } });

    if (!commentDB) {
      return null;
    }

    return new Comment({
      id: IdVO.createWithUUID(commentDB.id),
      message: MessageVO.create(commentDB.message),
      userId: IdVO.createWithUUID(commentDB.userId)
    });
  }

  async addComment(post: Post, comment: Comment): Promise<void> {
    const id: string = comment.id;
    const message: string = comment.message;
    const userId = comment.userId;
    const postId = post.id;

    await CommentModel.create({ id, message, userId, postId });
  }

  async updateComment(comment: Comment): Promise<void> {
    const commentDB: any = await CommentModel.findOne({ where: { id: comment.id } });
    await commentDB.update({
      message: comment.message
    });
  }

  async deleteComment(commentIdVO: IdVO): Promise<void> {
    await CommentModel.destroy({ where: { id: commentIdVO.value } });
  }

  async getPostComments(postDB: any): Promise<Comment[]> {
    return (await postDB.getComments()).map((c: any) => new Comment({
      id: IdVO.createWithUUID(c.id),
      userId: IdVO.createWithUUID(c.userId),
      message: MessageVO.create(c.message),
    }));
  }


  private mapPostDBToEntity(postDB: any, comments: Comment[]): Post {
    return new Post({
      id: IdVO.createWithUUID(postDB.id),
      title: TitleVO.create(postDB.title),
      content: ContentVO.create(postDB.content),
      userId: IdVO.createWithUUID(postDB.userId),
      comments: comments
    });
  }

}