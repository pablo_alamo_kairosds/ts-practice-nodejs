import { OffensiveWord } from '../../domain/model/entities/offensive-word.entity';
import { IdVO } from '../../domain/model/vos/id.vo';
import { LevelVO } from '../../domain/model/vos/level.vo';
import { WordVO } from '../../domain/model/vos/word.vo';
import { OffensiveWordRepository } from '../../domain/repositories/offensive-word.repository';
import { OffensiveWordModel } from './offensive-word.schema';

export class OffensiveWordRepositoryMongo implements OffensiveWordRepository {

  async save(offensiveWordEntity: OffensiveWord): Promise<void> {
    const offensiveWordModel = this.mapEntityToDB(offensiveWordEntity);
    await offensiveWordModel.save();
  }

  async getOneById(offensiveWordIdVO: IdVO): Promise<OffensiveWord | null> {
    const offensiveWordDB = await OffensiveWordModel.findOne({ id: offensiveWordIdVO.value }).exec();

    if (!offensiveWordDB) {
      return null;
    }

    return this.mapDBToEntity(offensiveWordDB);
  }

  async getOneByName(offensiveWordName: WordVO): Promise<OffensiveWord | null> {
    const offensiveWordDB = await OffensiveWordModel.findOne({ word: offensiveWordName.value }).exec();

    if (!offensiveWordDB) {
      return null;
    }

    return this.mapDBToEntity(offensiveWordDB);
  }

  async getAll(): Promise<OffensiveWord[]> {
    const allOffensiveWordsDB = await OffensiveWordModel.find({}).exec();
    return allOffensiveWordsDB.map(this.mapDBToEntity);
  }

  async delete(offensiveWordIdVO: IdVO): Promise<OffensiveWord> {
    const offensiveWordDB = await OffensiveWordModel.findOneAndDelete({ id: offensiveWordIdVO.value });
    return this.mapDBToEntity(offensiveWordDB);
  }

  async deleteMany(idsVOs: IdVO[]): Promise<number> {
    const ids = idsVOs.map(id => id.value);
    const { deletedCount } = await OffensiveWordModel.deleteMany({ id: { $in: ids }});
    return deletedCount;
  }

  async update(offensiveWordEntity: OffensiveWord): Promise<OffensiveWord> {
    const filter = { id: offensiveWordEntity.id };
    const update = { word: offensiveWordEntity.word, level: offensiveWordEntity.level };

    const offensiveWordDB = await OffensiveWordModel.findOneAndUpdate(filter, update, { new: true });

    return this.mapDBToEntity(offensiveWordDB);
  }

  private mapDBToEntity(offensiveWordDB: any): OffensiveWord {
    return new OffensiveWord({
      id: IdVO.createWithUUID(offensiveWordDB.id),
      word: WordVO.create(offensiveWordDB.word),
      level: LevelVO.create(offensiveWordDB.level)
    });
  }

  private mapEntityToDB(offensiveWordEntity: OffensiveWord): any {
    return new OffensiveWordModel({
      id: offensiveWordEntity.id,
      word: offensiveWordEntity.word,
      level: offensiveWordEntity.level
    });
  }

}