import { DataTypes } from 'sequelize';
import sequelize from '../config/postgresql';

export const PostModel = sequelize.define('posts', {

  id: {
    type: DataTypes.UUID,
    allowNull: false,
    primaryKey: true
  },
  title: {
    type: DataTypes.STRING,
    allowNull: false
  },
  content: {
    type: DataTypes.STRING(500),
    allowNull: false
  }

});