import { User, UserType } from '../../domain/model/entities/user.entity';
import { EmailVO } from '../../domain/model/vos/email.vo';
import { IdVO } from '../../domain/model/vos/id.vo';
import { PasswordVO } from '../../domain/model/vos/password.vo';
import { RoleVO } from '../../domain/model/vos/role.vo';
import { UserRepository } from '../../domain/repositories/user.repository';
import { logger } from '../config/logger';
import { UserModel } from './user.schema';

export class UserRepositoryPG implements UserRepository {

  async save(user: User): Promise<void> {
    const id: string = user.id;
    const email: string = user.email;
    const password: string = user.password;
    const role: string = user.role;

    await UserModel.create({ id, email, password, role });

    logger.debug(`Usuario persistido: ${JSON.stringify(user)}`);
  }

  async getByEmail(email: EmailVO): Promise<User | null> {
    const userDB: any = await UserModel.findOne({ where: { email: email.value } });

    if (!userDB) {
      return null;
    }

    const userData: UserType = {
      id: IdVO.createWithUUID(userDB.id),
      email: EmailVO.create(userDB.email),
      password: PasswordVO.create(userDB.password),
      role: RoleVO.create(userDB.role)
    };

    return new User(userData);
  }

  async delete(idVO: IdVO): Promise<void> {
    await UserModel.destroy({ where: { id: idVO.value } });
  }

  async getOneById(idVO: IdVO): Promise<User | null> {
    const userDB: any = await UserModel.findOne({ where: { id: idVO.value } });

    if (!userDB) {
      return null;
    }

    const userData: UserType = {
      id: IdVO.createWithUUID(userDB.id),
      email: EmailVO.create(userDB.email),
      password: PasswordVO.create(userDB.password),
      role: RoleVO.create(userDB.role)
    };

    return new User(userData);
  }

}