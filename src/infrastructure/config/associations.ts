import { CommentModel } from '../repositories/comment.schema';
import { PostModel } from '../repositories/post.schema';
import { UserModel } from '../repositories/user.schema';

export const createAssosationsPG = () => {
  UserModel.hasMany(PostModel, {
    foreignKey: { allowNull: false }
  });
  UserModel.hasMany(CommentModel, {
    foreignKey: { allowNull: false }
  });

  PostModel.belongsTo(UserModel);
  PostModel.hasMany(CommentModel, {
    onDelete: 'CASCADE',
    foreignKey: { allowNull: false }
  });

  CommentModel.belongsTo(PostModel);
  CommentModel.belongsTo(UserModel);
};