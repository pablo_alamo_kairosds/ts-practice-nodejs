import pino, { Logger, LoggerOptions } from 'pino';
import { context } from './async-context';

export const logger: Logger<LoggerOptions> = pino({
  name: 'blog-api',
  level: process.env.ENV === 'production' ? 'info': 'debug',
  mixin: () => ({
    correlationIdx: context.getStore() || 'noCorrelationId'
  })
  // Meter la clase donde se ejecuta
});