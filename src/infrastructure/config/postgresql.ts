import { Sequelize } from 'sequelize';

const host = process.env.PG_HOST ?? 'localhost',
  port = process.env.PG_PORT ?? '5432',
  dbName = process.env.PG_DB_NAME ?? 'pgdb',
  user = process.env.PG_USER ?? 'pguser',
  password = process.env.PG_PASSWORD ?? 'pgpass';

const sequelize = new Sequelize(`postgres://${ user }:${ password }@${ host }:${ port }/${ dbName }`);

export const init = async () => {
  try {
    await sequelize.authenticate();
    console.log('Connection has been established successfully.');
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
};

export default sequelize;
