import { createClient } from 'redis';

// redis[s]://[[username][:password]@][host][:port][/db-number]
const host = process.env.REDIS_HOST ?? 'localhost';
const port = process.env.REDIS_PORT ?? 6379;
const username = process.env.REDIS_USER ?? 'admin';
const password = process.env.REDIS_PASSWORD ?? 'admin';

export const client = createClient({
  url: `redis://${username}:${password}@${ host }:${ port }`
});

export const disconnect = (): void => {
  client.quit();
};
