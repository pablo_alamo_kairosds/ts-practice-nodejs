import Container from 'typedi';
import { CreateOffensiveWordUseCase } from '../../applications/usecases/offensive-word/create-offensive-word.usecase';
import { GetAllOffensiveWordsUseCase } from '../../applications/usecases/offensive-word/get-all-offensive-words.usecase';
import { OffensiveWordInput } from '../../applications/usecases/offensive-word/offensive-word.input';
import { OffensiveWordOutput } from '../../applications/usecases/offensive-word/offensive-word.output';
import { UserType } from '../../domain/model/entities/user.entity';
import { EmailVO } from '../../domain/model/vos/email.vo';
import { IdVO } from '../../domain/model/vos/id.vo';
import { PasswordVO } from '../../domain/model/vos/password.vo';
import { Role, RoleVO } from '../../domain/model/vos/role.vo';
import { UserService } from '../../domain/services/user.service';

const populate = async (): Promise<void> => {

  // USERS
  const userService = Container.get(UserService);
  const admin: UserType = {
    id: IdVO.create(),
    email: EmailVO.create('admin@admin.com'),
    password: PasswordVO.create('admin'),
    role: RoleVO.create(Role.ADMIN)
  };
  await userService.persist(admin);

  // OFFENSIVE WORDS
  const getAllOWsUseCase = Container.get(GetAllOffensiveWordsUseCase);
  const allOffensiveWords: OffensiveWordOutput[] = await getAllOWsUseCase.execute();

  if (!allOffensiveWords.length) {
    const DEFAULT_OWs: OffensiveWordInput[] = [
      { word: 'fart', level: 1 },
      { word: 'ass', level: 4 },
      { word: 'troll', level: 3 },
      { word: 'pussy', level: 5 },
      { word: 'god', level: 1 },
    ];
    const createUseCase = Container.get(CreateOffensiveWordUseCase);
    DEFAULT_OWs.forEach(async (offensiveWord) => {
      await createUseCase.execute(offensiveWord);
    });
  }

};

export { populate as populateDatabases };
