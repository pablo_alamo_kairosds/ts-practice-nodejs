import { NextFunction, Request, Response } from 'express';
import Container from 'typedi';
import { GetAllPostsUseCase } from '../../../applications/usecases/post/get-all-posts.usecase';
import { PostOutput } from '../../../applications/usecases/post/post.output';

export const getAllPostsController = () => async (req: Request, res: Response, next: NextFunction): Promise<Response | void> => {
  
  try {
    const useCase = Container.get(GetAllPostsUseCase);
    const posts: PostOutput[] = await useCase.execute();

    res.locals.data = posts;
    return res.status(200).json(posts);

  } catch (err) {
    next(err);
  }
  
};