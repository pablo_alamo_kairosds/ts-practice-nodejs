import { NextFunction, Request, Response } from 'express';
import Container from 'typedi';
import { IdRequest } from '../../../applications/usecases/id.request';
import { CommentOutput } from '../../../applications/usecases/post/comment.output';
import { UpdateCommentInput, UpdateCommentUseCase } from '../../../applications/usecases/post/update-comment.usecase';
import { User } from '../../../domain/model/entities/user.entity';

export const updateCommentController = () => async (req: Request, res: Response, next: NextFunction): Promise<Response | void> => {
    
  try {
    const user = req.user as User;
    const userId: IdRequest = user.id;
    const postId: IdRequest = req.params.postId;
    const commentId: IdRequest = req.params.commentId;
    const { message } = req.body;

    const updateInput: UpdateCommentInput = { commentId, postId, userId, message };

    const useCase = Container.get(UpdateCommentUseCase);
    const comment: CommentOutput = await useCase.execute(updateInput);

    return res.status(200).json(comment);
  } catch (err) {
    next(err);
  }
};