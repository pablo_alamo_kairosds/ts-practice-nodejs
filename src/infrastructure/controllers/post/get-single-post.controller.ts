import { NextFunction, Request, Response } from 'express';
import Container from 'typedi';
import { IdRequest } from '../../../applications/usecases/id.request';
import { GetOnePostUseCase } from '../../../applications/usecases/post/get-one-post.usecase';
import { PostOutput } from '../../../applications/usecases/post/post.output';

export const getSinglePostController = () => async (req: Request, res: Response, next: NextFunction): Promise<Response | void> => {
  try {
    const postIdReq: IdRequest = req.params.postId;
    const useCase = Container.get(GetOnePostUseCase);
    const post: PostOutput = await useCase.execute(postIdReq);

    res.locals.data = post;
    return res.status(200).json(post);
    
  } catch (err) {
    next(err);
  }
};