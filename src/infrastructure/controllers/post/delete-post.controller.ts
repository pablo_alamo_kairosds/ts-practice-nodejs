import { NextFunction, Request, Response } from 'express';
import Container from 'typedi';
import { IdRequest } from '../../../applications/usecases/id.request';
import { DeletePostUseCase } from '../../../applications/usecases/post/delete-post.usecase';
import { PostOutput } from '../../../applications/usecases/post/post.output';

export const deletePostController = () => async (req: Request, res: Response, next: NextFunction): Promise<Response | void> => {

  try {
    const postIdReq: IdRequest = req.params.postId;
    const useCase = Container.get(DeletePostUseCase);
    const post: PostOutput = await useCase.execute(postIdReq);

    return res.status(200).json(post);
  } catch (err) {
    next(err);
  }
};