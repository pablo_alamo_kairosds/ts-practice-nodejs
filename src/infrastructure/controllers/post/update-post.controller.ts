import { NextFunction, Request, Response } from 'express';
import Container from 'typedi';
import { IdRequest } from '../../../applications/usecases/id.request';
import { PostOutput } from '../../../applications/usecases/post/post.output';
import { UpdatePostInput, UpdatePostUseCase } from '../../../applications/usecases/post/update-post.usecase';

export const updatePostController = () => async (req: Request, res: Response, next: NextFunction): Promise<Response | void> => {
    
  try {
    const postId: IdRequest = req.params.postId;
    const { title, content } = req.body;

    const updateInput: UpdatePostInput = { postId, title, content };

    const useCase = Container.get(UpdatePostUseCase);
    const post: PostOutput = await useCase.execute(updateInput);

    return res.status(200).json(post);
  } catch (err) {
    next(err);
  }
};