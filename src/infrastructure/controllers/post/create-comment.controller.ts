import { NextFunction, Request, Response } from 'express';
import Container from 'typedi';
import { IdRequest } from '../../../applications/usecases/id.request';
import { CommentInput } from '../../../applications/usecases/post/comment.input';
import { CommentOutput } from '../../../applications/usecases/post/comment.output';
import { CreateCommentUseCase } from '../../../applications/usecases/post/create-comment.usecase';
import { User } from '../../../domain/model/entities/user.entity';

export const createCommentController = () => async (req: Request, res: Response, next: NextFunction): Promise<Response | void> => {

  try {
    const user = req.user as User;
    const userId: IdRequest = user.id;
    const postId: IdRequest = req.params.postId;
    const { message } = req.body;

    const commentInput: CommentInput = { message, userId, postId };

    const useCase = Container.get(CreateCommentUseCase);
    const newComment: CommentOutput = await useCase.execute(commentInput);

    return res.status(201).json(newComment);

  } catch (err) {
    next(err);
  }
};