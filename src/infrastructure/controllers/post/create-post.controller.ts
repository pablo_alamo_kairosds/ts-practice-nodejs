import { NextFunction, Request, Response } from 'express';
import Container from 'typedi';
import { CreatePostUseCase } from '../../../applications/usecases/post/create-post.usecase';
import { PostInput } from '../../../applications/usecases/post/post.input';
import { PostOutput } from '../../../applications/usecases/post/post.output';
import { DomainForbiddenException } from '../../../domain/errors/domain-forbidden.exception';
import { User } from '../../../domain/model/entities/user.entity';
import { Role } from '../../../domain/model/vos/role.vo';

export const createPostController = () => async (req: Request, res: Response, next: NextFunction): Promise<Response | void> => {

  try {
    const user = req.user as User;
    const { title, content, authorId } = req.body;

    let userId: string = user.id;
    
    if (authorId && userId !== authorId && user.role !== Role.ADMIN) {
      throw new DomainForbiddenException();
    }

    if (user.role === Role.ADMIN && authorId) {
      userId = authorId;
    }

    const postInput: PostInput = { title, content, userId };
    const useCase = Container.get(CreatePostUseCase);
    const newPost: PostOutput = await useCase.execute(postInput);

    return res.status(201).json(newPost);

  } catch (err) {
    next(err);
  }
};