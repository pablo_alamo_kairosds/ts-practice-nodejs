import { NextFunction, Request, Response } from 'express';
import Container from 'typedi';
import { IdRequest } from '../../../applications/usecases/id.request';
import { DeleteCommentInput, DeleteCommentUseCase } from '../../../applications/usecases/post/delete-comment.usecase';

export const deleteCommentController = () => async (req: Request, res: Response, next: NextFunction): Promise<Response | void> => {

  try {
    const postId: IdRequest = req.params.postId;
    const commentId: IdRequest = req.params.commentId;

    const deleteCommentInput: DeleteCommentInput = { postId, commentId };

    const useCase = Container.get(DeleteCommentUseCase);
    await useCase.execute(deleteCommentInput);

    res.status(200).json({ status: 'Comment deleted' });

  } catch (err) {
    next(err);
  }
};