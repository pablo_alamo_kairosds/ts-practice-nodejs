import { NextFunction, Request, Response } from 'express';
import Container from 'typedi';
import { CreateOffensiveWordUseCase } from '../../../applications/usecases/offensive-word/create-offensive-word.usecase';
import { OffensiveWordInput } from '../../../applications/usecases/offensive-word/offensive-word.input';
import { OffensiveWordOutput } from '../../../applications/usecases/offensive-word/offensive-word.output';

export const createOffensiveWordController = () => async (req: Request, res: Response, next: NextFunction): Promise<Response | void> => {

  try {
    const { word, level } = req.body;
    const offensiveWordInput: OffensiveWordInput = { word, level };
    const useCase = Container.get(CreateOffensiveWordUseCase);
    const newOffensiveWord: OffensiveWordOutput = await useCase.execute(offensiveWordInput);

    return res.status(201).json(newOffensiveWord);
  } catch (err) {
    next(err);
  }
};