import { NextFunction, Response } from 'express';
import Container from 'typedi';
import { IdRequest } from '../../../applications/usecases/id.request';
import { DeleteManyOffensiveWordUseCase } from '../../../applications/usecases/offensive-word/delete-many-offensive-word.usecase';

export const deleteManyOffensiveWordController = () => async (req: { query: { ids: string; }; }, res: Response, next: NextFunction): Promise<Response | void> => {
    
  try {
    const idsReq: IdRequest[] = req.query.ids.split(',');
    const useCase = Container.get(DeleteManyOffensiveWordUseCase);
    const deletedCount: number = await useCase.execute(idsReq);

    return res.status(200).json({ message: `${ deletedCount } offensive words have been deleted` });
  } catch (err) {
    next(err);
  }
};