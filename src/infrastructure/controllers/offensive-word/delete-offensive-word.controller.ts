import { NextFunction, Request, Response } from 'express';
import Container from 'typedi';
import { IdRequest } from '../../../applications/usecases/id.request';
import { DeleteOffensiveWordUseCase } from '../../../applications/usecases/offensive-word/delete-offensive-word.usecase';
import { OffensiveWordOutput } from '../../../applications/usecases/offensive-word/offensive-word.output';

export const deleteOffensiveWordController = () => async (req: Request, res: Response, next: NextFunction): Promise<Response | void> => {

  try {
    const idReq: IdRequest = req.params.id;
    const useCase = Container.get(DeleteOffensiveWordUseCase);
    const offensiveWord: OffensiveWordOutput = await useCase.execute(idReq);

    return res.status(200).json(offensiveWord);
  } catch (err) {
    next(err);
  }
};