import { NextFunction, Request, Response } from 'express';
import Container from 'typedi';
import { IdRequest } from '../../../applications/usecases/id.request';
import { OffensiveWordInput } from '../../../applications/usecases/offensive-word/offensive-word.input';
import { OffensiveWordOutput } from '../../../applications/usecases/offensive-word/offensive-word.output';
import { UpdateOffensiveWordUseCase } from '../../../applications/usecases/offensive-word/update-offensive-word.usecase';

export const updateOffensiveWordController = () => async (req: Request, res: Response, next: NextFunction): Promise<Response | void> => {

  try {
    const idReq: IdRequest = req.params.id;
    const { word, level } = req.body;
    const offensiveWordInput: OffensiveWordInput = { word, level };

    const useCase = Container.get(UpdateOffensiveWordUseCase);
    const offensiveWordUpdated: OffensiveWordOutput = await useCase.execute(idReq, offensiveWordInput);

    return res.status(200).json(offensiveWordUpdated);
  } catch (err) {
    next(err);
  }
};