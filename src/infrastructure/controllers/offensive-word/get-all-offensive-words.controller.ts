import { Request, Response } from 'express';
import Container from 'typedi';
import { GetAllOffensiveWordsUseCase } from '../../../applications/usecases/offensive-word/get-all-offensive-words.usecase';
import { OffensiveWordOutput } from '../../../applications/usecases/offensive-word/offensive-word.output';

export const getAllOffensiveWordsController = () => async (req: Request, res: Response): Promise<Response | void> => {
  const useCase = Container.get(GetAllOffensiveWordsUseCase);
  const allOffensiveWords: OffensiveWordOutput[] = await useCase.execute();
  return res.status(200).json(allOffensiveWords);
};