import { NextFunction, Request, Response } from 'express';
import Container from 'typedi';
import { SignUpInput, SignUpUseCase } from '../../../applications/usecases/auth/sign-up.usecase';
import { UserOutput } from '../../../applications/usecases/auth/user.output';

export const signUpController = () => async (req: Request, res: Response, next: NextFunction): Promise<Response | void> => {
  try {
    const { email, password, role } = req.body;
    const signUpInput: SignUpInput = { email, password, role };
    const useCase = Container.get(SignUpUseCase);
    const newUser: UserOutput = await useCase.execute(signUpInput);

    return res.status(201).json(newUser);

  } catch (err) {
    next(err);
  }
};