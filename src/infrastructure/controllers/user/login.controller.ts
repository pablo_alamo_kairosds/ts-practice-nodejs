import { NextFunction, Request, Response } from 'express';
import Container from 'typedi';
import { LoginInput, LoginUseCase } from '../../../applications/usecases/auth/login.usecase';

export const loginController = () => async (req: Request, res: Response, next: NextFunction): Promise<Response | void> => {
  
  try {
    const { email, password } = req.body;
    const signInInput: LoginInput = { email, password };
    const useCase = Container.get(LoginUseCase);
    const token: string = await useCase.execute(signInInput);

    return res.status(200).json({ token });

  } catch (err) {
    next(err);
  }

};