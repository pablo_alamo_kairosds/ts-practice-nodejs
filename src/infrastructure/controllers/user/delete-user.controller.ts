import { NextFunction, Request, Response } from 'express';
import Container from 'typedi';
import { DeleteUserUseCase } from '../../../applications/usecases/auth/delete-user.usecase';
import { UserOutput } from '../../../applications/usecases/auth/user.output';
import { IdRequest } from '../../../applications/usecases/id.request';

export const deleteUserController = () => async (req: Request, res: Response, next: NextFunction): Promise<Response | void> => {
  try {
    const userId: IdRequest = req.params.userId;
    const useCase = Container.get(DeleteUserUseCase);
    const deletedUser: UserOutput = await useCase.execute(userId);

    return res.status(200).json(deletedUser);
  } catch (err) {
    next(err);
  }
};