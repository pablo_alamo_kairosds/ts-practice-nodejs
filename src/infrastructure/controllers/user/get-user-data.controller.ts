import { Request, Response } from 'express';
import { User } from '../../../domain/model/entities/user.entity';

export const getUserDataController = () => (req: Request, res: Response) => {
  const { role, id } = req.user as User;
  return res.status(200).json({ role, id });
};