import { Service } from 'typedi';
import { User } from '../../../domain/model/entities/user.entity';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { UserService } from '../../../domain/services/user.service';
import { logger } from '../../../infrastructure/config/logger';
import { IdRequest } from '../id.request';
import { UserOutput } from './user.output';

@Service()
export class DeleteUserUseCase {

  constructor(private userService: UserService) {}
  
  async execute(userId: IdRequest): Promise<UserOutput> {

    logger.debug('Ejecutando DeleteUserUseCase');
    
    const idVO = IdVO.createWithUUID(userId);
    const user: User = await this.userService.delete(idVO);

    logger.debug(`Usuario eliminado: ${JSON.stringify(user)}`);

    const output: UserOutput = {
      id: user.id,
      email: user.email,
    };

    return output;
  }
  
}