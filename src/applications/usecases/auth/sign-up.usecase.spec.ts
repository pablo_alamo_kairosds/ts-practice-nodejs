jest.mock('../../../infrastructure/repositories/user.repository.pg', () => {
  return {
    UserRepositoryPG: jest.fn().mockImplementation(() => {
      return {
        save: jest.fn(),
        getByEmail: jest.fn()
      };
    })
  };
});

import 'reflect-metadata';
import Container from 'typedi';
import { Role } from '../../../domain/model/vos/role.vo';
import { UserRepositoryPG } from '../../../infrastructure/repositories/user.repository.pg';
import { SignUpInput, SignUpUseCase } from './sign-up.usecase';
import { UserOutput } from './user.output';

describe('Sign up use case', () => {

  it('should create user and persist', async () => {

    const repository = new UserRepositoryPG();
    Container.set('UserRepository', repository);

    const useCase = Container.get(SignUpUseCase);
    const userReq: SignUpInput = {
      email: 'test@gmail.com',
      password: 'password',
      role: Role.AUTHOR
    };
    const newUser: UserOutput = await useCase.execute(userReq);

    expect(newUser.email).toEqual('test@gmail.com');
    expect(repository.save).toHaveBeenCalled();
    expect(repository.getByEmail).toHaveBeenCalled();
  });

});