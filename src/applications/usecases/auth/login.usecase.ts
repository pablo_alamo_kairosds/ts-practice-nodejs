import jwt from 'jsonwebtoken';
import { Service } from 'typedi';
import { DomainNotFoundException } from '../../../domain/errors/domain-not-found.exception';
import { User } from '../../../domain/model/entities/user.entity';
import { EmailVO } from '../../../domain/model/vos/email.vo';
import { PasswordVO } from '../../../domain/model/vos/password.vo';
import { UserService } from '../../../domain/services/user.service';
import { logger } from '../../../infrastructure/config/logger';

@Service()
export class LoginUseCase {

  constructor(private userService: UserService) { }

  async execute(req: LoginInput): Promise<string> {

    logger.debug('Ejecutando LoginUseCase');
    
    const user: User | null = await this.userService.getByEmail(EmailVO.create(req.email));

    if (!user) {
      throw new DomainNotFoundException('User', req.email);
    }
    
    const plainPassword = PasswordVO.create(req.password);
    await this.userService.isValidPassword(plainPassword, user);

    logger.debug(`Usuario logeado: ${JSON.stringify(user)}`);

    const secret = process.env.SECRET ?? 'secret';
    return jwt.sign({ email: user.email }, secret, {
      expiresIn: 86400 // 24 hours
    });

  }

}

export type LoginInput = {
  email: string;
  password: string;
};