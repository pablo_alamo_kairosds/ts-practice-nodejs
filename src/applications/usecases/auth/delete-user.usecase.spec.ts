const mockedMethod = jest.fn().mockImplementation(() => {
  const userData: UserType = {
    id: IdVO.create(),
    email: EmailVO.create('test@test.com'),
    password: PasswordVO.create('test'),
    role: RoleVO.create(Role.USER)
  };

  return new User(userData);
});

jest.mock('../../../infrastructure/repositories/user.repository.pg', () => {
  return {
    UserRepositoryPG: jest.fn().mockImplementation(() => {
      return {
        delete: mockedMethod,
        getOneById: mockedMethod
      };
    })
  };
});

import 'reflect-metadata';
import Container from 'typedi';
import { User, UserType } from '../../../domain/model/entities/user.entity';
import { EmailVO } from '../../../domain/model/vos/email.vo';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { PasswordVO } from '../../../domain/model/vos/password.vo';
import { Role, RoleVO } from '../../../domain/model/vos/role.vo';
import { UserRepositoryPG } from '../../../infrastructure/repositories/user.repository.pg';
import { IdRequest } from '../id.request';
import { DeleteUserUseCase } from './delete-user.usecase';
import { UserOutput } from './user.output';

describe('Delete user use case', () => {

  it('should delete user', async () => {

    const repository = new UserRepositoryPG();
    Container.set('UserRepository', repository);

    const useCase = Container.get(DeleteUserUseCase);
    const idReq: IdRequest = IdVO.create().value;

    const user: UserOutput = await useCase.execute(idReq);

    expect(user.id).toBeTruthy();
    expect(user.email).toBeTruthy();
    expect(repository.delete).toHaveBeenCalled();
    expect(repository.getOneById).toHaveBeenCalled();
  });

});