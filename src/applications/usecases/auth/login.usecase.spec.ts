const mockedMethod = jest.fn().mockImplementation(() => {
  const userData: UserType = {
    id: IdVO.create(),
    email: EmailVO.create('test@test.com'),
    password: PasswordVO.create('test'),
    role: RoleVO.create(Role.USER)
  };

  return new User(userData);
});

jest.mock('../../../infrastructure/repositories/user.repository.pg', () => {
  return {
    UserRepositoryPG: jest.fn().mockImplementation(() => {
      return {
        getByEmail: mockedMethod
      };
    })
  };
});

import 'reflect-metadata';
import Container from 'typedi';
import { DomainUnauthorizedException } from '../../../domain/errors/domain-unauthorized.exception';
import { User, UserType } from '../../../domain/model/entities/user.entity';
import { EmailVO } from '../../../domain/model/vos/email.vo';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { PasswordVO } from '../../../domain/model/vos/password.vo';
import { Role, RoleVO } from '../../../domain/model/vos/role.vo';
import { UserRepositoryPG } from '../../../infrastructure/repositories/user.repository.pg';
import { LoginInput, LoginUseCase } from './login.usecase';

describe('Login use case', () => {

  it('should throw error', async () => {

    const repository = new UserRepositoryPG();
    Container.set('UserRepository', repository);

    const useCase = Container.get(LoginUseCase);
    const userReq: LoginInput = {
      email: 'test@gmail.com',
      password: 'password'
    };

    try {
      await useCase.execute(userReq);
    } catch (err) {
      if (err instanceof DomainUnauthorizedException) {
        expect(err.message).toEqual('Invalid login or password.');
      }
    }

    expect(repository.getByEmail).toHaveBeenCalled();
  });

});