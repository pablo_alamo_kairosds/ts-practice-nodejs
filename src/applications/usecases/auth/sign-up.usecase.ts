import { Service } from 'typedi';
import { UserType } from '../../../domain/model/entities/user.entity';
import { EmailVO } from '../../../domain/model/vos/email.vo';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { PasswordVO } from '../../../domain/model/vos/password.vo';
import { Role, RoleVO } from '../../../domain/model/vos/role.vo';
import { UserService } from '../../../domain/services/user.service';
import { logger } from '../../../infrastructure/config/logger';
import { UserOutput } from './user.output';

@Service()
export class SignUpUseCase {

  constructor(private userService: UserService) {}

  async execute(req: SignUpInput): Promise<UserOutput> {

    logger.debug(`Ejecutando ${this.constructor.name}`);
    
    const userData: UserType = {
      id: IdVO.create(),
      email: EmailVO.create(req.email),
      password: PasswordVO.create(req.password),
      role: RoleVO.create(Role[req.role])
    };
    
    await this.userService.persist(userData);
    
    logger.debug(`Creado el ${req.role} ${req.email}`);

    const output: UserOutput = {
      id: userData.id.value,
      email: userData.email.value
    };

    return output; 
  }
  
}

export type SignUpInput = {
  email: string;
  password: string;
  role: 'AUTHOR' | 'USER';
}