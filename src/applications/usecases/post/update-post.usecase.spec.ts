jest.mock('../../../infrastructure/repositories/post.repository.pg', () => {
  return {
    PostRepositoryPG: jest.fn().mockImplementation(() => {
      return {
        updatePost: jest.fn(),
        getPostById: jest.fn().mockImplementation(() =>
          new Post({
            id: IdVO.create(),
            userId: IdVO.create(),
            title: TitleVO.create('testing'),
            content: ContentVO.create('testingtestingtestingtestingtestingtestingtestingtesting'),
            comments: []
          })
        )
      };
    })
  };
});

import 'reflect-metadata';
import Container from 'typedi';
import { Post } from '../../../domain/model/entities/post.entity';
import { ContentVO } from '../../../domain/model/vos/content.vo';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { TitleVO } from '../../../domain/model/vos/title.vo';
import { PostRepositoryPG } from '../../../infrastructure/repositories/post.repository.pg';
import { PostOutput } from './post.output';
import { UpdatePostInput, UpdatePostUseCase } from './update-post.usecase';

describe('Update Post use case', () => {

  it('should update post', async () => {

    const repository = new PostRepositoryPG();
    Container.set('PostRepository', repository);

    const useCase = Container.get(UpdatePostUseCase);
    const input: UpdatePostInput = {
      postId: IdVO.create().value,
      title: 'Testing',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris'
    };
    const post: PostOutput = await useCase.execute(input);

    expect(post.id).toBeTruthy();
    expect(post.userId).toBeTruthy();
    expect(post.title).toBeTruthy();
    expect(post.content).toBeTruthy();

    expect(repository.getPostById).toHaveBeenCalled();
    expect(repository.updatePost).toHaveBeenCalled();
  });

});