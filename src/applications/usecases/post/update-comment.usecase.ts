import { Service } from 'typedi';
import { CommentType } from '../../../domain/model/entities/comment.entity';
import { OffensiveWord } from '../../../domain/model/entities/offensive-word.entity';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { MessageVO } from '../../../domain/model/vos/message.vo';
import { OffensiveWordService } from '../../../domain/services/offensive-word.service';
import { PostService } from '../../../domain/services/post.service';
import { logger } from '../../../infrastructure/config/logger';
import { CommentOutput } from './comment.output';

@Service()
export class UpdateCommentUseCase {

  constructor(private postService: PostService,
    private offensiveWordService: OffensiveWordService) { }

  async execute(updateData: UpdateCommentInput): Promise<CommentOutput> {

    logger.debug('Ejecutando UpdateCommentUseCase');

    const offensiveWords: OffensiveWord[] = await this.offensiveWordService.getAll();

    const commentData: CommentType = {
      id: IdVO.createWithUUID(updateData.commentId),
      userId: IdVO.createWithUUID(updateData.userId),
      message: MessageVO.create(updateData.message, offensiveWords)
    };
    const postId = IdVO.createWithUUID(updateData.postId);

    await this.postService.updateComment(postId, commentData);

    const output: CommentOutput = {
      id: commentData.id.value,
      userId: commentData.userId.value,
      message: commentData.message.value
    };

    logger.debug(`Comentario modificado: ${JSON.stringify(output)}`);

    return output;
  }

}

export type UpdateCommentInput = {
  postId: string,
  userId: string,
  commentId: string,
  message: string,
};