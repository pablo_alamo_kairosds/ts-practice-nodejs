import { Service } from 'typedi';
import { Post, PostType } from '../../../domain/model/entities/post.entity';
import { ContentVO } from '../../../domain/model/vos/content.vo';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { TitleVO } from '../../../domain/model/vos/title.vo';
import { PostService } from '../../../domain/services/post.service';
import { logger } from '../../../infrastructure/config/logger';
import { PostOutput } from './post.output';

@Service()
export class UpdatePostUseCase {

  constructor(private postService: PostService) { }

  async execute(updateData: UpdatePostInput): Promise<PostOutput> {

    logger.debug('Ejecutando UpdatePostUseCase');

    const postData: Partial<PostType> = {
      id: IdVO.createWithUUID(updateData.postId),
      title: TitleVO.create(updateData.title),
      content: ContentVO.create(updateData.content),
    };
    
    const post: Post = await this.postService.updatePost(postData);
    
    logger.debug(`Post modificado: ${JSON.stringify(post)}`);

    const output: PostOutput = {
      id: post.id,
      userId: post.userId,
      title: post.title,
      content: post.content
    };
    return output;
  }

}

export type UpdatePostInput = {
  postId: string,
  title: string,
  content: string,
};