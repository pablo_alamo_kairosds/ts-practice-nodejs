jest.mock('../../../infrastructure/repositories/post.repository.pg', () => {
  return {
    PostRepositoryPG: jest.fn().mockImplementation(() => {
      return {
        deletePost: jest.fn(),
        getPostById: jest.fn().mockImplementation(() =>
          new Post({
            id: IdVO.create(),
            userId: IdVO.create(),
            title: TitleVO.create('Lorem ipsum'),
            content: ContentVO.create('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
            comments: []
          })
        )
      };
    })
  };
});

import 'reflect-metadata';
import Container from 'typedi';
import { Post } from '../../../domain/model/entities/post.entity';
import { ContentVO } from '../../../domain/model/vos/content.vo';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { TitleVO } from '../../../domain/model/vos/title.vo';
import { PostRepositoryPG } from '../../../infrastructure/repositories/post.repository.pg';
import { IdRequest } from '../id.request';
import { DeletePostUseCase } from './delete-post.usecase';
import { PostOutput } from './post.output';

describe('Delete post use case', () => {

  it('should delete a post', async () => {

    // Inyectamos el repository de mongo
    const repository = new PostRepositoryPG();
    Container.set('PostRepository', repository);

    // ejecutamos el use case
    const useCase = Container.get(DeletePostUseCase);
    const postId: IdRequest = IdVO.create().value;
    const deletedPost: PostOutput = await useCase.execute(postId);

    expect(deletedPost.id).toBeTruthy();
    expect(deletedPost.title).toBeTruthy();
    expect(deletedPost.content).toBeTruthy();
    expect(deletedPost.userId).toBeTruthy();
    expect(deletedPost.comments).toBeTruthy();
    
    expect(repository.deletePost).toHaveBeenCalled();
    expect(repository.getPostById).toHaveBeenCalled();

  });

});