import { Service } from 'typedi';
import { Comment } from '../../../domain/model/entities/comment.entity';
import { Post } from '../../../domain/model/entities/post.entity';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { PostService } from '../../../domain/services/post.service';
import { logger } from '../../../infrastructure/config/logger';
import { IdRequest } from '../id.request';
import { PostOutput } from './post.output';

@Service()
export class GetOnePostUseCase {

  constructor(private postService: PostService) { }

  async execute(postId: IdRequest): Promise<PostOutput> {

    logger.debug('Ejecutando GetOnePostUseCase');

    const postIdVO = IdVO.createWithUUID(postId);
    const post: Post = await this.postService.getPostById(postIdVO);

    const output: PostOutput = {
      id: post.id,
      userId: post.userId,
      title: post.title,
      content: post.content,
      comments: post.comments.map((c: Comment) => ({
        id: c.id,
        userId: c.userId,
        message: c.message
      }))
    };

    logger.debug(`Post pedido: ${ JSON.stringify(output) }`);

    return output;
  }

}