jest.mock('../../../infrastructure/repositories/post.repository.pg', () => {
  return {
    PostRepositoryPG: jest.fn().mockImplementation(() => {
      return {
        addComment: jest.fn(),
        getPostById: jest.fn().mockImplementation(() =>
          new Post({
            id: IdVO.create(),
            userId: IdVO.create(),
            title: TitleVO.create('Lorem ipsum'),
            content: ContentVO.create('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
            comments: []
          })
        )
      };
    })
  };
});

jest.mock('../../../infrastructure/repositories/offensive-word.repository.mongo', () => {
  return {
    OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
      return {
        getAll: jest.fn().mockImplementation(() => [1, 2, 3, 4, 5].map((num) =>
          new OffensiveWord({
            id: IdVO.create(),
            word: WordVO.create(`test${ num }`),
            level: LevelVO.create(num)
          })
        ))
      };
    })
  };
});

import 'reflect-metadata';
import Container from 'typedi';
import { OffensiveWord } from '../../../domain/model/entities/offensive-word.entity';
import { Post } from '../../../domain/model/entities/post.entity';
import { ContentVO } from '../../../domain/model/vos/content.vo';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { LevelVO } from '../../../domain/model/vos/level.vo';
import { TitleVO } from '../../../domain/model/vos/title.vo';
import { WordVO } from '../../../domain/model/vos/word.vo';
import { OffensiveWordRepositoryMongo } from '../../../infrastructure/repositories/offensive-word.repository.mongo';
import { PostRepositoryPG } from '../../../infrastructure/repositories/post.repository.pg';
import { CommentInput } from './comment.input';
import { CommentOutput } from './comment.output';
import { CreateCommentUseCase } from './create-comment.usecase';

describe('Create comment use case', () => {

  it('should create a comment and persist', async () => {

    const postRepository = new PostRepositoryPG();
    Container.set('PostRepository', postRepository);
    const offensiveWordRepository = new OffensiveWordRepositoryMongo();
    Container.set('OffensiveWordRepository', offensiveWordRepository);
    
    const useCase = Container.get(CreateCommentUseCase);
    const commentInput: CommentInput = {
      userId: IdVO.create().value,
      postId: IdVO.create().value,
      message: 'testingtestingtestingtesting'
    };
    const newComment: CommentOutput = await useCase.execute(commentInput);

    expect(newComment.id).toBeTruthy();
    expect(newComment.userId).toBeTruthy();
    expect(newComment.message).toBeTruthy();
    
    expect(postRepository.addComment).toHaveBeenCalled();
    expect(postRepository.getPostById).toHaveBeenCalled();
    expect(offensiveWordRepository.getAll).toHaveBeenCalled();
  });

});