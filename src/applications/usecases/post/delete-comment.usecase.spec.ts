jest.mock('../../../infrastructure/repositories/post.repository.pg', () => {
  return {
    PostRepositoryPG: jest.fn().mockImplementation(() => {
      return {
        deleteComment: jest.fn(),
        getPostById: jest.fn().mockImplementation(() =>
          new Post({
            id: IdVO.create(),
            userId: IdVO.create(),
            title: TitleVO.create('Lorem ipsum'),
            content: ContentVO.create('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
            comments: []
          })
        )
      };
    })
  };
});

import 'reflect-metadata';
import Container from 'typedi';
import { Post } from '../../../domain/model/entities/post.entity';
import { ContentVO } from '../../../domain/model/vos/content.vo';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { TitleVO } from '../../../domain/model/vos/title.vo';
import { PostRepositoryPG } from '../../../infrastructure/repositories/post.repository.pg';
import { DeleteCommentInput, DeleteCommentUseCase } from './delete-comment.usecase';

describe('Delete comment use case', () => {

  it('should delete a comment', async () => {

    const postRepository = new PostRepositoryPG();
    Container.set('PostRepository', postRepository);

    const useCase = Container.get(DeleteCommentUseCase);
    const deleteData: DeleteCommentInput = {
      commentId: IdVO.create().value,
      postId: IdVO.create().value,
    };
    await useCase.execute(deleteData);

    expect(postRepository.deleteComment).toHaveBeenCalled();
    expect(postRepository.getPostById).toHaveBeenCalled();
  });

});