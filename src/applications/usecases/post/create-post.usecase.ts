import { Service } from 'typedi';
import { DomainNotFoundException } from '../../../domain/errors/domain-not-found.exception';
import { PostType } from '../../../domain/model/entities/post.entity';
import { User } from '../../../domain/model/entities/user.entity';
import { ContentVO } from '../../../domain/model/vos/content.vo';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { TitleVO } from '../../../domain/model/vos/title.vo';
import { PostService } from '../../../domain/services/post.service';
import { UserService } from '../../../domain/services/user.service';
import { logger } from '../../../infrastructure/config/logger';
import { PostInput } from './post.input';
import { PostOutput } from './post.output';

@Service()
export class CreatePostUseCase {

  constructor(private postService: PostService, private authService: UserService) { }

  async execute(post: PostInput): Promise<PostOutput> {

    logger.debug('Ejecutando CreatePostUseCase');

    const userIdVO = IdVO.createWithUUID(post.userId);
    const author: User | null = await this.authService.getOneById(userIdVO);
    if (!author) {
      throw new DomainNotFoundException('User', userIdVO.value);
    }
    
    const postData: PostType = {
      id: IdVO.create(),
      userId: userIdVO,
      title: TitleVO.create(post.title),
      content: ContentVO.create(post.content),
      comments: []
    };

    await this.postService.persist(postData);

    const output: PostOutput = {
      id: postData.id.value,
      userId: postData.userId.value,
      title: postData.title.value,
      content: postData.content.value,
      comments: []
    };

    logger.debug(`Post creado: ${ JSON.stringify(output) }`);

    return output;
  }

}