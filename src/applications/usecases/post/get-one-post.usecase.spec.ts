jest.mock('../../../infrastructure/repositories/post.repository.pg', () => {
  return {
    PostRepositoryPG: jest.fn().mockImplementation(() => {
      return {
        getPostById: jest.fn().mockImplementation(() =>
          new Post({
            id: IdVO.create(),
            userId: IdVO.create(),
            title: TitleVO.create('testing'),
            content: ContentVO.create('testingtestingtestingtestingtestingtestingtestingtesting'),
            comments: []
          })
        )
      };
    })
  };
});

import 'reflect-metadata';
import Container from 'typedi';
import { Post } from '../../../domain/model/entities/post.entity';
import { ContentVO } from '../../../domain/model/vos/content.vo';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { TitleVO } from '../../../domain/model/vos/title.vo';
import { PostRepositoryPG } from '../../../infrastructure/repositories/post.repository.pg';
import { IdRequest } from '../id.request';
import { GetOnePostUseCase } from './get-one-post.usecase';
import { PostOutput } from './post.output';

describe('Get Post use case', () => {

  it('should get post by id', async () => {

    const repository = new PostRepositoryPG();
    Container.set('PostRepository', repository);

    const useCase = Container.get(GetOnePostUseCase);
    const idReq: IdRequest = IdVO.create().value;
    const post: PostOutput = await useCase.execute(idReq);

    expect(post.title).toBeTruthy();
    expect(post.content).toBeTruthy();
    expect(post.comments).toBeTruthy();
    expect(repository.getPostById).toHaveBeenCalled();
  });

});