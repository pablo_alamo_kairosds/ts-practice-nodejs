import { CommentOutput } from './comment.output';

export type PostOutput = {
  id: string,
  userId: string,
  title: string,
  content: string,
  comments?: CommentOutput[];
};
