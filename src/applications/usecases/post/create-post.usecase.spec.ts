jest.mock('../../../infrastructure/repositories/post.repository.pg');
jest.mock('../../../infrastructure/repositories/user.repository.pg', () => {
  return {
    UserRepositoryPG: jest.fn().mockImplementation(() => {
      return {
        getOneById: jest.fn().mockImplementation(() => {
          const userData: UserType = {
            id: IdVO.create(),
            email: EmailVO.create('test@test.com'),
            password: PasswordVO.create('test'),
            role: RoleVO.create(Role.AUTHOR)
          };

          return new User(userData);
        })
      };
    })
  };
});

import 'reflect-metadata';
import Container from 'typedi';
import { UserType, User } from '../../../domain/model/entities/user.entity';
import { EmailVO } from '../../../domain/model/vos/email.vo';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { PasswordVO } from '../../../domain/model/vos/password.vo';
import { RoleVO, Role } from '../../../domain/model/vos/role.vo';
import { PostRepositoryPG } from '../../../infrastructure/repositories/post.repository.pg';
import { UserRepositoryPG } from '../../../infrastructure/repositories/user.repository.pg';
import { CreatePostUseCase } from './create-post.usecase';
import { PostInput } from './post.input';
import { PostOutput } from './post.output';

describe('Create post use case', () => {

  it('should create a post and persist', async () => {

    // Inyectamos el repository de posts y user
    const postRepository = new PostRepositoryPG();
    Container.set('PostRepository', postRepository);
    const userRepository = new UserRepositoryPG();
    Container.set('UserRepository', userRepository);

    // ejecutamos el use case
    const useCase = Container.get(CreatePostUseCase);
    const postInput: PostInput = {
      title: 'Lorem ipsum',
      content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate',
      userId: IdVO.create().value
    };
    const newPost: PostOutput = await useCase.execute(postInput);

    expect(newPost.id).toBeTruthy();
    expect(newPost.title).toEqual(postInput.title);
    expect(newPost.content).toEqual(postInput.content);
    expect(newPost.userId).toEqual(postInput.userId);
    expect(newPost.comments).toBeTruthy();
    expect(userRepository.getOneById).toHaveBeenCalled();
    expect(postRepository.savePost).toHaveBeenCalled();

  });

});