import { Service } from 'typedi';
import { CommentType } from '../../../domain/model/entities/comment.entity';
import { OffensiveWord } from '../../../domain/model/entities/offensive-word.entity';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { MessageVO } from '../../../domain/model/vos/message.vo';
import { OffensiveWordService } from '../../../domain/services/offensive-word.service';
import { PostService } from '../../../domain/services/post.service';
import { logger } from '../../../infrastructure/config/logger';
import { CommentInput } from './comment.input';
import { CommentOutput } from './comment.output';

@Service()
export class CreateCommentUseCase {

  constructor(private postService: PostService,
    private offensiveWordService: OffensiveWordService) { }

  async execute(comment: CommentInput): Promise<CommentOutput> {

    logger.debug('Ejecutando CreateCommentUseCase');

    const postIdVO: IdVO = IdVO.createWithUUID(comment.postId);

    const offensiveWords: OffensiveWord[] = await this.offensiveWordService.getAll();

    const commentData: CommentType = {
      id: IdVO.create(),
      message: MessageVO.create(comment.message, offensiveWords),
      userId: IdVO.createWithUUID(comment.userId)
    };

    await this.postService.addComment(postIdVO, commentData);

    const output: CommentOutput = {
      id: commentData.id.value,
      message: commentData.message.value,
      userId: commentData.userId.value
    };

    logger.debug(`Comentario creado: ${ JSON.stringify(output) }`);

    return output;
  }

}