import { Service } from 'typedi';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { PostService } from '../../../domain/services/post.service';
import { logger } from '../../../infrastructure/config/logger';

@Service()
export class DeleteCommentUseCase {

  constructor(private postService: PostService) { }

  async execute(deleteData: DeleteCommentInput): Promise<void> {

    logger.debug('Ejecutando DeleteCommentUseCase');

    const postIdVO = IdVO.createWithUUID(deleteData.postId);
    const commentIdVO = IdVO.createWithUUID(deleteData.commentId);

    logger.debug(`Comentario eliminado: ${ deleteData.commentId }`);

    await this.postService.deleteComment(postIdVO, commentIdVO);
  }

}

export type DeleteCommentInput = {
  postId: string,
  commentId: string;
};