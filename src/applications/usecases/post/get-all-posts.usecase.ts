import { Service } from 'typedi';
import { Post } from '../../../domain/model/entities/post.entity';
import { PostService } from '../../../domain/services/post.service';
import { logger } from '../../../infrastructure/config/logger';
import { PostOutput } from './post.output';

@Service()
export class GetAllPostsUseCase {

  constructor(private postService: PostService) { }

  async execute(): Promise<PostOutput[]> {

    logger.debug('Ejecutando GetAllPostsUseCase');

    const posts: Post[] = await this.postService.getAllPosts();

    const output: PostOutput[] = posts.map((post: Post) => {
      const postOutput: PostOutput = {
        id: post.id,
        userId: post.userId,
        title: post.title,
        content: post.content
      };
      return postOutput;
    });

    return output;
  }

}