export type CommentOutput = {
  id: string,
  message: string,
  userId: string
}