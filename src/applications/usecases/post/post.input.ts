export type PostInput = {
  title: string,
  content: string,
  userId: string
}