import { Service } from 'typedi';
import { Comment } from '../../../domain/model/entities/comment.entity';
import { Post } from '../../../domain/model/entities/post.entity';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { PostService } from '../../../domain/services/post.service';
import { logger } from '../../../infrastructure/config/logger';
import { IdRequest } from '../id.request';
import { PostOutput } from './post.output';

@Service()
export class DeletePostUseCase {

  constructor(private postService: PostService) { }

  async execute(postId: IdRequest): Promise<PostOutput> {

    logger.debug('Ejecutando DeletePostUseCase');

    const idVO = IdVO.createWithUUID(postId);
    const deletedPost: Post = await this.postService.deletePost(idVO);

    const output: PostOutput = {
      id: deletedPost.id,
      userId: deletedPost.userId,
      title: deletedPost.title,
      content: deletedPost.content,
      comments: deletedPost.comments.map((c: Comment) => ({
        id: c.id,
        userId: c.userId,
        message: c.message
      }))
    };

    logger.debug(`Post eliminado: ${ JSON.stringify(output) }`);

    return output;
  }

}