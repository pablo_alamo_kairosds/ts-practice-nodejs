export type CommentInput = {
  message: string,
  userId: string,
  postId: string
}