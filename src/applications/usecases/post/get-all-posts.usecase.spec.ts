jest.mock('../../../infrastructure/repositories/post.repository.pg', () => {
  return {
    PostRepositoryPG: jest.fn().mockImplementation(() => {
      return {
        getAllPosts: jest.fn().mockImplementation(() => {
          return [
            new Post({
              id: IdVO.create(),
              title: TitleVO.create('Lorem ipsum'),
              content: ContentVO.create('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'),
              userId: IdVO.create(),
              comments: []
            })
          ];
        })
      };
    })
  };
});

import 'reflect-metadata';
import Container from 'typedi';
import { Post } from '../../../domain/model/entities/post.entity';
import { ContentVO } from '../../../domain/model/vos/content.vo';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { TitleVO } from '../../../domain/model/vos/title.vo';
import { PostRepositoryPG } from '../../../infrastructure/repositories/post.repository.pg';
import { GetAllPostsUseCase } from './get-all-posts.usecase';
import { PostOutput } from './post.output';

describe('Gell list of all posts use case', () => {

  it('should get all posts', async () => {

    const repository = new PostRepositoryPG();
    Container.set('PostRepository', repository);

    const useCase = Container.get(GetAllPostsUseCase);
    const posts: PostOutput[] = await useCase.execute();

    expect(posts).toHaveLength(1);
    expect(posts[0].title).toBeTruthy();
    expect(posts[0].content).toBeTruthy();
    expect(repository.getAllPosts).toHaveBeenCalled();

  });

});