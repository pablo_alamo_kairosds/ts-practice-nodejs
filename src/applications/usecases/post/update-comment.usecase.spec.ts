jest.mock('../../../infrastructure/repositories/post.repository.pg', () => {
  return {
    PostRepositoryPG: jest.fn().mockImplementation(() => {
      return {
        updateComment: jest.fn(),
        getPostById: jest.fn().mockImplementation(() =>
          new Post({
            id: IdVO.create(),
            userId: IdVO.create(),
            title: TitleVO.create('testing'),
            content: ContentVO.create('testingtestingtestingtestingtestingtestingtestingtesting'),
            comments: []
          })
        )
      };
    })
  };
});

jest.mock('../../../infrastructure/repositories/offensive-word.repository.mongo', () => {
  return {
    OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
      return {
        getAll: jest.fn().mockImplementation(() => [1, 2, 3, 4, 5].map((num) =>
          new OffensiveWord({
            id: IdVO.create(),
            word: WordVO.create(`test${ num }`),
            level: LevelVO.create(num)
          })
        ))
      };
    })
  };
});

import 'reflect-metadata';
import Container from 'typedi';
import { OffensiveWord } from '../../../domain/model/entities/offensive-word.entity';
import { Post } from '../../../domain/model/entities/post.entity';
import { ContentVO } from '../../../domain/model/vos/content.vo';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { LevelVO } from '../../../domain/model/vos/level.vo';
import { TitleVO } from '../../../domain/model/vos/title.vo';
import { WordVO } from '../../../domain/model/vos/word.vo';
import { OffensiveWordRepositoryMongo } from '../../../infrastructure/repositories/offensive-word.repository.mongo';
import { PostRepositoryPG } from '../../../infrastructure/repositories/post.repository.pg';
import { CommentOutput } from './comment.output';
import { UpdateCommentInput, UpdateCommentUseCase } from './update-comment.usecase';

describe('Update Comment use case', () => {

  it('should update comment', async () => {

    const postRepository = new PostRepositoryPG();
    Container.set('PostRepository', postRepository);
    const offensiveWordRepository = new OffensiveWordRepositoryMongo();
    Container.set('OffensiveWordRepository', offensiveWordRepository);

    const useCase = Container.get(UpdateCommentUseCase);
    const input: UpdateCommentInput = {
      postId: IdVO.create().value,
      userId: IdVO.create().value,
      commentId: IdVO.create().value,
      message: 'testing',
    };
    const comment: CommentOutput = await useCase.execute(input);

    expect(comment.id).toBeTruthy();
    expect(comment.userId).toBeTruthy();
    expect(comment.message).toBeTruthy();

    expect(postRepository.getPostById).toHaveBeenCalled();
    expect(postRepository.updateComment).toHaveBeenCalled();
    expect(offensiveWordRepository.getAll).toHaveBeenCalled();
  });

});