jest.mock('../../../infrastructure/repositories/offensive-word.repository.mongo');

import 'reflect-metadata';
import Container from 'typedi';
import { OffensiveWordRepositoryMongo } from '../../../infrastructure/repositories/offensive-word.repository.mongo';
import { CreateOffensiveWordUseCase } from './create-offensive-word.usecase';
import { OffensiveWordInput } from './offensive-word.input';
import { OffensiveWordOutput } from './offensive-word.output';

describe('Create offensive word use case', () => {

  it('should create word and persist', async () => {

    // Inyectamos el repository de mongo
    const repository = new OffensiveWordRepositoryMongo();
    Container.set('OffensiveWordRepository', repository);
    
    // ejecutamos el use case
    const useCase = Container.get(CreateOffensiveWordUseCase);
    const offensiveWordInput: OffensiveWordInput = {
      word: 'test',
      level: 5
    };
    const newOffensiveWord: OffensiveWordOutput = await useCase.execute(offensiveWordInput);

    expect(newOffensiveWord.word).toEqual(offensiveWordInput.word);
    expect(newOffensiveWord.level).toEqual(offensiveWordInput.level);
    expect(repository.save).toHaveBeenCalled();

  });

});