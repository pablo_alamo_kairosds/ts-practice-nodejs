import { Service } from 'typedi';
import { OffensiveWord } from '../../../domain/model/entities/offensive-word.entity';
import { OffensiveWordService } from '../../../domain/services/offensive-word.service';
import { logger } from '../../../infrastructure/config/logger';
import { OffensiveWordOutput } from './offensive-word.output';

@Service()
export class GetAllOffensiveWordsUseCase {

  constructor(private offensiveWordService: OffensiveWordService) {}
  
  async execute(): Promise<OffensiveWordOutput[]> {
    
    logger.debug('Ejecutando GetAllOffensiveWordsUseCase');

    const offensiveWords: OffensiveWord[] = await this.offensiveWordService.getAll();

    return (offensiveWords.map((offensiveWord: OffensiveWord) => ({
      id: offensiveWord.id,
      word: offensiveWord.word,
      level: offensiveWord.level
    } as OffensiveWordOutput)));
  }
  
}
