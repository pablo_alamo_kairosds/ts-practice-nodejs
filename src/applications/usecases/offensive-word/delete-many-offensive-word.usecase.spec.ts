jest.mock('../../../infrastructure/repositories/offensive-word.repository.mongo', () => {
  return {
    OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
      return {
        deleteMany: jest.fn().mockReturnValue(3)
      };
    })
  };
});

import 'reflect-metadata';
import Container from 'typedi';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { OffensiveWordRepositoryMongo } from '../../../infrastructure/repositories/offensive-word.repository.mongo';
import { IdRequest } from '../id.request';
import { DeleteManyOffensiveWordUseCase } from './delete-many-offensive-word.usecase';

describe('Delete offensive word use case', () => {

  it('should delete word by id', async () => {

    // Inyectamos el repository de mongo
    const repository = new OffensiveWordRepositoryMongo();
    Container.set('OffensiveWordRepository', repository);

    // ejecutamos el use case
    const useCase = Container.get(DeleteManyOffensiveWordUseCase);
    const idsReq: IdRequest[] = [0, 1, 2].map(() => IdVO.create().value);
    const deletedCount: number = await useCase.execute(idsReq);

    expect(deletedCount).toEqual(3);
    expect(repository.deleteMany).toHaveBeenCalled();
  });

});