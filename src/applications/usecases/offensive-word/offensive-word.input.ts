export type OffensiveWordInput = {
  word: string;
  level: number;
}