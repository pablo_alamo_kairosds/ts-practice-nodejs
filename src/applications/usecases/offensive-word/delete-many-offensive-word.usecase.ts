import { Service } from 'typedi';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { OffensiveWordService } from '../../../domain/services/offensive-word.service';
import { logger } from '../../../infrastructure/config/logger';
import { IdRequest } from '../id.request';

@Service()
export class DeleteManyOffensiveWordUseCase {

  constructor(private offensiveWordService: OffensiveWordService) {}
  
  async execute(ids: IdRequest[]): Promise<number> {

    logger.debug('Ejecutando DeleteManyOffensiveWordUseCase');

    const idsVOs = ids.map(IdVO.createWithUUID);
    const deletedCount: number = await this.offensiveWordService.deleteMany(idsVOs);

    logger.debug(`${deletedCount} offensive words eliminadas`);

    return deletedCount;
  }
  
}