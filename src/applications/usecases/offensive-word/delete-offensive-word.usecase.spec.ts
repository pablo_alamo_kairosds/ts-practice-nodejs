const mockedMethod = jest.fn().mockImplementation(() =>
  new OffensiveWord({
    id: IdVO.createWithUUID('e68aa30a-f3dd-4c89-a101-8eca5786e672'),
    word: WordVO.create('test'),
    level: LevelVO.create(5)
  })
);

jest.mock('../../../infrastructure/repositories/offensive-word.repository.mongo', () => {
  return {
    OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
      return {
        delete: mockedMethod,
        getOneById: mockedMethod
      };
    })
  };
});

import 'reflect-metadata';
import Container from 'typedi';
import { OffensiveWord } from '../../../domain/model/entities/offensive-word.entity';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { LevelVO } from '../../../domain/model/vos/level.vo';
import { WordVO } from '../../../domain/model/vos/word.vo';
import { OffensiveWordRepositoryMongo } from '../../../infrastructure/repositories/offensive-word.repository.mongo';
import { IdRequest } from '../id.request';
import { DeleteOffensiveWordUseCase } from './delete-offensive-word.usecase';
import { OffensiveWordOutput } from './offensive-word.output';

describe('Delete offensive word use case', () => {

  it('should delete word by id', async () => {

    // Inyectamos el repository de mongo
    const repository = new OffensiveWordRepositoryMongo();
    Container.set('OffensiveWordRepository', repository);

    // ejecutamos el use case
    const useCase = Container.get(DeleteOffensiveWordUseCase);
    const idReq: IdRequest = IdVO.createWithUUID('e68aa30a-f3dd-4c89-a101-8eca5786e672').value;
    const deletedOffensiveWord: OffensiveWordOutput = await useCase.execute(idReq);

    expect(deletedOffensiveWord.id).toEqual(idReq);
    expect(deletedOffensiveWord.word).toEqual('test');
    expect(deletedOffensiveWord.level).toEqual(5);
    expect(repository.delete).toHaveBeenCalled();

  });

});