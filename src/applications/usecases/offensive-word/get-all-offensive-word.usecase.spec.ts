jest.mock('../../../infrastructure/repositories/offensive-word.repository.mongo', () => {
  return {
    OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
      return {
        getAll: jest.fn().mockImplementation(() => [1, 2, 3, 4, 5].map((num) =>
          new OffensiveWord({
            id: IdVO.create(),
            word: WordVO.create(`test${ num }`),
            level: LevelVO.create(num)
          })
        ))
      };
    })
  };
});

import 'reflect-metadata';
import Container from 'typedi';
import { OffensiveWord } from '../../../domain/model/entities/offensive-word.entity';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { LevelVO } from '../../../domain/model/vos/level.vo';
import { WordVO } from '../../../domain/model/vos/word.vo';
import { OffensiveWordRepositoryMongo } from '../../../infrastructure/repositories/offensive-word.repository.mongo';
import { GetAllOffensiveWordsUseCase } from './get-all-offensive-words.usecase';
import { OffensiveWordOutput } from './offensive-word.output';

describe('Gell list of offensive words use case', () => {

  it('should get all offensive words', async () => {

    // Inyectamos el repository de mongo
    const repository = new OffensiveWordRepositoryMongo();
    Container.set('OffensiveWordRepository', repository);

    // ejecutamos el use case
    const useCase = Container.get(GetAllOffensiveWordsUseCase);
    const offensiveWords: OffensiveWordOutput[] = await useCase.execute();

    expect(offensiveWords).toHaveLength(5);
    expect(offensiveWords[0].word).toEqual('test1');
    expect(offensiveWords[0].level).toEqual(1);
    expect(offensiveWords[4].word).toEqual('test5');
    expect(offensiveWords[4].level).toEqual(5);
    expect(repository.getAll).toHaveBeenCalled();

  });

});