import { Service } from 'typedi';
import { OffensiveWord } from '../../../domain/model/entities/offensive-word.entity';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { OffensiveWordService } from '../../../domain/services/offensive-word.service';
import { logger } from '../../../infrastructure/config/logger';
import { IdRequest } from '../id.request';
import { OffensiveWordOutput } from './offensive-word.output';

@Service()
export class DeleteOffensiveWordUseCase {

  constructor(private offensiveWordService: OffensiveWordService) {}
  
  async execute(offensiveWordId: IdRequest): Promise<OffensiveWordOutput> {

    logger.debug('Ejecutando DeleteOffensiveWordUseCase');

    const idVO = IdVO.createWithUUID(offensiveWordId);
    const deletedOffensiveWord: OffensiveWord = await this.offensiveWordService.delete(idVO);

    logger.debug(`Offensive word eliminada: ${JSON.stringify(deletedOffensiveWord)}`);

    const output: OffensiveWordOutput = {
      id: deletedOffensiveWord.id,
      word: deletedOffensiveWord.word,
      level: deletedOffensiveWord.level
    };

    return output;
  }
  
}