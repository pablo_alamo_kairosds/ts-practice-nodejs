import { Service } from 'typedi';
import { OffensiveWordType } from '../../../domain/model/entities/offensive-word.entity';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { LevelVO } from '../../../domain/model/vos/level.vo';
import { WordVO } from '../../../domain/model/vos/word.vo';
import { OffensiveWordService } from '../../../domain/services/offensive-word.service';
import { logger } from '../../../infrastructure/config/logger';
import { OffensiveWordInput } from './offensive-word.input';
import { OffensiveWordOutput } from './offensive-word.output';

// El caso de uso puede ser usado desde la propia aplicación, desde el api REST o de donde sea, está desacoplado
// No depende de la forma en la que mi app persiste los datos
@Service()
export class CreateOffensiveWordUseCase {

  constructor(private offensiveWordService: OffensiveWordService) {}
  
  // Me llega algo "input" y si devuelvo algo "output" pero no tiene porque haber una API de por medio
  async execute(offensiveWordRequest: OffensiveWordInput): Promise<OffensiveWordOutput> {

    logger.debug('Ejecutando CreateOffensiveWordUseCase');

    const offensiveWordData: OffensiveWordType = {
      id: IdVO.create(),
      word: WordVO.create(offensiveWordRequest.word),
      level: LevelVO.create(offensiveWordRequest.level)
    };
    await this.offensiveWordService.persist(offensiveWordData);

    const output: OffensiveWordOutput = {
      id: offensiveWordData.id.value,
      word: offensiveWordData.word.value,
      level: offensiveWordData.level.value
    };

    logger.debug(`Offensive word creada: ${JSON.stringify(output)}`);

    return output;
  }
  
}