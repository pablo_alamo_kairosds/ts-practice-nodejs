export type OffensiveWordOutput = {
  id: string;
  word: string;
  level: number;
}