import { Service } from 'typedi';
import { OffensiveWord, OffensiveWordType } from '../../../domain/model/entities/offensive-word.entity';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { LevelVO } from '../../../domain/model/vos/level.vo';
import { WordVO } from '../../../domain/model/vos/word.vo';
import { OffensiveWordService } from '../../../domain/services/offensive-word.service';
import { logger } from '../../../infrastructure/config/logger';
import { IdRequest } from '../id.request';
import { OffensiveWordInput } from './offensive-word.input';
import { OffensiveWordOutput } from './offensive-word.output';

@Service()
export class UpdateOffensiveWordUseCase {

  constructor(private offensiveWordService: OffensiveWordService) { }

  async execute(offensiveWordId: IdRequest, offensiveWordInput: OffensiveWordInput): Promise<OffensiveWordOutput> {

    logger.debug('Ejecutando UpdateOffensiveWordUseCase');

    const offensiveWordData: OffensiveWordType = {
      id: IdVO.createWithUUID(offensiveWordId),
      word: WordVO.create(offensiveWordInput.word),
      level: LevelVO.create(offensiveWordInput.level)
    };
    const updatedOffensiveWord: OffensiveWord = await this.offensiveWordService.update(offensiveWordData);

    logger.debug(`Offensive word modificada: ${JSON.stringify(updatedOffensiveWord)}`);

    const output: OffensiveWordOutput = {
      id: updatedOffensiveWord.id,
      word: updatedOffensiveWord.word,
      level: updatedOffensiveWord.level
    };

    return output;
  }

}