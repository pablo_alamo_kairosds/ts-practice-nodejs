const mockedMethod = jest.fn().mockImplementation(() =>
  new OffensiveWord({
    id: IdVO.createWithUUID('e68aa30a-f3dd-4c89-a101-8eca5786e672'),
    word: WordVO.create('test'),
    level: LevelVO.create(5)
  })
);

jest.mock('../../../infrastructure/repositories/offensive-word.repository.mongo', () => {
  return {
    OffensiveWordRepositoryMongo: jest.fn().mockImplementation(() => {
      return {
        update: mockedMethod,
        getOneById: mockedMethod,
        getOneByName: jest.fn().mockReturnValue(null)
      };
    })
  };
});

import 'reflect-metadata';
import Container from 'typedi';
import { OffensiveWord } from '../../../domain/model/entities/offensive-word.entity';
import { IdVO } from '../../../domain/model/vos/id.vo';
import { LevelVO } from '../../../domain/model/vos/level.vo';
import { WordVO } from '../../../domain/model/vos/word.vo';
import { OffensiveWordRepositoryMongo } from '../../../infrastructure/repositories/offensive-word.repository.mongo';
import { IdRequest } from '../id.request';
import { UpdateOffensiveWordUseCase } from './update-offensive-word.usecase';
import { OffensiveWordOutput } from './offensive-word.output';
import { OffensiveWordInput } from './offensive-word.input';

describe('Update offensive word use case', () => {

  it('should update word by id, word and level', async () => {

    // Inyectamos el repository de mongo
    const repository = new OffensiveWordRepositoryMongo();
    Container.set('OffensiveWordRepository', repository);

    // ejecutamos el use case
    const useCase = Container.get(UpdateOffensiveWordUseCase);
    const idReq: IdRequest = IdVO.createWithUUID('e68aa30a-f3dd-4c89-a101-8eca5786e672').value;
    const offensiveWordInput: OffensiveWordInput = { word: 'test', level: 5 };
    const updatedOffensiveWord: OffensiveWordOutput = await useCase.execute(idReq, offensiveWordInput);

    expect(updatedOffensiveWord.id).toEqual(idReq);
    expect(updatedOffensiveWord.word).toEqual('test');
    expect(updatedOffensiveWord.level).toEqual(5);
    expect(repository.update).toHaveBeenCalled();

  });

});