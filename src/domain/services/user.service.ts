import bcrypt from 'bcrypt';
import { Inject, Service } from 'typedi';
import { DomainConflictException } from '../errors/domain-conflict.exception';
import { DomainNotFoundException } from '../errors/domain-not-found.exception';
import { DomainUnauthorizedException } from '../errors/domain-unauthorized.exception';
import { User, UserType } from '../model/entities/user.entity';
import { EmailVO } from '../model/vos/email.vo';
import { IdVO } from '../model/vos/id.vo';
import { PasswordVO } from '../model/vos/password.vo';
import { UserRepository } from '../repositories/user.repository';

@Service()
export class UserService {

  constructor(@Inject('UserRepository') private userRepository: UserRepository) { }

  private async checkIfEmailInUse(email: EmailVO): Promise<void> {
    const user: User | null = await this.userRepository.getByEmail(email);
    if (user) {
      throw new DomainConflictException('email', email.value);
    }
  }

  private async checkIfIdExists(idVO: IdVO): Promise<User> {
    const user = await this.getOneById(idVO);
    if (!user) {
      throw new DomainNotFoundException('User', idVO.value);
    }
    return user;
  }

  async isValidPassword(password: PasswordVO, user: User): Promise<void> {
    const isValidPassword = await bcrypt.compare(password.value, user.password);
    if (!isValidPassword) {
      throw new DomainUnauthorizedException('Invalid login or password.');
    }
  }

  async persist(user: UserType): Promise<void> {
    await this.checkIfEmailInUse(user.email);

    const hash = await bcrypt.hash(user.password.value, 10);
    const encryptPassword = PasswordVO.create(hash);
    const newUser: UserType = {
      id: user.id,
      email: user.email,
      password: encryptPassword,
      role: user.role
    };
    const userEntity = new User(newUser);
    await this.userRepository.save(userEntity);
  }

  async getByEmail(email: EmailVO): Promise<User | null> {
    const user: User | null = await this.userRepository.getByEmail(email);
    if (!user) {
      return null;
    }
    return user;
  }

  async getOneById(idVO: IdVO): Promise<User | null> {
    return await this.userRepository.getOneById(idVO);
  }

  async delete(idVO: IdVO): Promise<User> {
    const user: User = await this.checkIfIdExists(idVO);
    await this.userRepository.delete(idVO);
    return user;
  }

}