import { Inject, Service } from 'typedi';
import { DomainNotFoundException } from '../errors/domain-not-found.exception';
import { Comment, CommentType } from '../model/entities/comment.entity';
import { Post, PostType } from '../model/entities/post.entity';
import { ContentVO } from '../model/vos/content.vo';
import { IdVO } from '../model/vos/id.vo';
import { TitleVO } from '../model/vos/title.vo';
import { PostRepository } from '../repositories/post.repository';

@Service()
export class PostService {

  constructor(@Inject('PostRepository') private postRepository: PostRepository) { }

  async persist(post: PostType): Promise<void> {
    await this.postRepository.savePost(new Post(post));
  }

  async deletePost(postIdVO: IdVO): Promise<Post> {
    const post = await this.getPostById(postIdVO);
    await this.postRepository.deletePost(postIdVO);
    return post;
  }

  async updatePost(postData: Partial<PostType>): Promise<Post> {
    const originalPost = await this.getPostById(postData.id!);

    const postMerge: PostType = {
      id: postData.id!,
      title: TitleVO.create(postData.title!.value),
      content: ContentVO.create(postData.content!.value),
      userId: IdVO.createWithUUID(originalPost.userId),
      comments: originalPost.comments
    };
    const postMergeEntity = new Post(postMerge);

    await this.postRepository.updatePost(postMergeEntity);
    return postMergeEntity;
  }

  async getAllPosts(): Promise<Post[]> {
    return await this.postRepository.getAllPosts();
  }

  async addComment(postIdVO: IdVO, comment: CommentType): Promise<void> {
    const post = await this.getPostById(postIdVO);
    await this.postRepository.addComment(post, new Comment(comment));
  }

  async updateComment(postIdVO: IdVO, comment: CommentType): Promise<void> {
    await this.getPostById(postIdVO);
    await this.postRepository.updateComment(new Comment(comment));
  }

  async deleteComment(postIdVO: IdVO, commentIdVO: IdVO): Promise<void> {
    await this.getPostById(postIdVO);
    await this.postRepository.deleteComment(commentIdVO);
  }

  async getCommentById(commentIdVO: IdVO): Promise<Comment> {
    const comment: Comment | null = await this.postRepository.getCommentById(commentIdVO);

    if (!comment) {
      throw new DomainNotFoundException('Comment', commentIdVO.value);
    }
    return comment;
  }

  async getPostById(postIdVO: IdVO): Promise<Post> {
    const post: Post | null = await this.postRepository.getPostById(postIdVO);

    if (!post) {
      throw new DomainNotFoundException('Post', postIdVO.value);
    }
    return post;
  }

}