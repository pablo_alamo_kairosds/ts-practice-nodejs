import { OffensiveWord } from '../model/entities/offensive-word.entity';
import { IdVO } from '../model/vos/id.vo';
import { LevelVO } from '../model/vos/level.vo';
import { WordVO } from '../model/vos/word.vo';
import { countOffensiveWords } from './count-offensive-words.service';

describe('Count Offensive Words in Message', () => {
  
  it('should count 7 offensive words', () => {
    const fakeOffensiveWords: OffensiveWord[] = [
      new OffensiveWord({ id: IdVO.create(), word: WordVO.create('fart'), level: LevelVO.create(4) }),
      new OffensiveWord({ id: IdVO.create(), word: WordVO.create('ass'), level: LevelVO.create(1) }),
      new OffensiveWord({ id: IdVO.create(), word: WordVO.create('troll'), level: LevelVO.create(3) }),
      new OffensiveWord({ id: IdVO.create(), word: WordVO.create('pussy'), level: LevelVO.create(2) }),
      new OffensiveWord({ id: IdVO.create(), word: WordVO.create('noob'), level: LevelVO.create(5) }),
      new OffensiveWord({ id: IdVO.create(), word: WordVO.create('fuck'), level: LevelVO.create(5) })
    ];
    
    const message = 'Onli.ne    Java-script  troll  Edi+tor for  "fuck" fuck  free"" Write, &Edit €€and Run $your Javascript code using- #pussy# JS Online Compiler noob+ ass€ fart&';
    const count = countOffensiveWords(message, fakeOffensiveWords);

    expect(count).toEqual(7);
  });

  it('should count 1 offensive words', () => { 
    const fakeOffensiveWords: OffensiveWord[] = [
      new OffensiveWord({ id: IdVO.create(), word: WordVO.create('fart'), level: LevelVO.create(4) }),
    ];

    const message = '+{}fart%·"%&$/';
    const count = countOffensiveWords(message, fakeOffensiveWords);

    expect(count).toEqual(1);
  });

  it('should not count any offensive words', () => { 
    const message = 'fart ass troll pussy noob fuck';
    const count = countOffensiveWords(message, []);

    expect(count).toEqual(0);
  });

});