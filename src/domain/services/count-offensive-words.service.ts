import { OffensiveWord } from '../model/entities/offensive-word.entity';

export const countOffensiveWords = (message: string, offensiveWords: OffensiveWord[]): number => {

  const words = message.toLowerCase().replace(/[.,€$/#!'·$%^&*;:"'{}=+_`~()]/g, '').split(' ');
  const offensiveWordsList = offensiveWords.map(ow => ow.word);

  return words.reduce((acc, word) => offensiveWordsList.includes(word) ? acc + 1 : acc, 0);
};