import { Inject, Service } from 'typedi';
import { DomainConflictException } from '../errors/domain-conflict.exception';
import { DomainNotFoundException } from '../errors/domain-not-found.exception';
import { OffensiveWord, OffensiveWordType } from '../model/entities/offensive-word.entity';
import { IdVO } from '../model/vos/id.vo';
import { WordVO } from '../model/vos/word.vo';
import { OffensiveWordRepository } from '../repositories/offensive-word.repository';

// Los servicios ejecutan acciones con las interfaces y los vos
@Service()
export class OffensiveWordService {

  // Inyectamos la intefaz 
  constructor(@Inject('OffensiveWordRepository') private offensiveWordRepository: OffensiveWordRepository) { }

  async persist(offensiveWord: OffensiveWordType): Promise<void> {
    await this.getByName(offensiveWord.word);
    const wordEntity = new OffensiveWord(offensiveWord);
    await this.offensiveWordRepository.save(wordEntity);
  }

  async getAll(): Promise<OffensiveWord[]> {
    return await this.offensiveWordRepository.getAll();
  }

  async delete(offensiveWordIdVO: IdVO): Promise<OffensiveWord> {
    await this.getById(offensiveWordIdVO);
    return await this.offensiveWordRepository.delete(offensiveWordIdVO);
  }

  async deleteMany(idsVOs: IdVO[]): Promise<number> {
    const deletedCount = await this.offensiveWordRepository.deleteMany(idsVOs);
    if (deletedCount === 0) {
      throw new DomainNotFoundException('Words');
    }
    return deletedCount;
  }

  async update(offensiveWord: OffensiveWordType): Promise<OffensiveWord> {
    await this.getById(offensiveWord.id);
    await this.getByName(offensiveWord.word, offensiveWord.id);
    const wordEntity = new OffensiveWord(offensiveWord);
    return await this.offensiveWordRepository.update(wordEntity);
  }

  private async getById(offensiveWordIdVO: IdVO): Promise<OffensiveWord> {
    const offensiveWord: OffensiveWord | null = await this.offensiveWordRepository.getOneById(offensiveWordIdVO);
    if (!offensiveWord) {
      throw new DomainNotFoundException('Id', offensiveWordIdVO.value);
    }
    return offensiveWord;
  }

  private async getByName(offensiveWordName: WordVO, id?: IdVO): Promise<void> {
    const offensiveWord: OffensiveWord | null = await this.offensiveWordRepository.getOneByName(offensiveWordName);
    if (offensiveWord && offensiveWord.id !== id?.value) {
      throw new DomainConflictException('word', offensiveWordName.value);
    }
  }

}