import { OffensiveWord } from '../model/entities/offensive-word.entity';
import { IdVO } from '../model/vos/id.vo';
import { WordVO } from '../model/vos/word.vo';

export interface OffensiveWordRepository {

  save(offensiveWord: OffensiveWord): Promise<void>;

  getAll(): Promise<OffensiveWord[]>;

  getOneById(offensiveWordId: IdVO): Promise<OffensiveWord | null>;

  getOneByName(offensiveWordName: WordVO): Promise<OffensiveWord | null>;

  delete(offensiveWordId: IdVO): Promise<OffensiveWord>;

  deleteMany(idsVOs: IdVO[]): Promise<number>;

  update(offensiveWord: OffensiveWord): Promise<OffensiveWord>;

}