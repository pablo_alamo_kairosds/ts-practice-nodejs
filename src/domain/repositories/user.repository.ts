import { User } from '../model/entities/user.entity';
import { EmailVO } from '../model/vos/email.vo';
import { IdVO } from '../model/vos/id.vo';

export interface UserRepository {

  save(user: User): Promise<void>;

  getByEmail(email: EmailVO): Promise<User | null>;
  
  delete(userId: IdVO): Promise<void>;

  getOneById(userId: IdVO): Promise<User | null>;

}