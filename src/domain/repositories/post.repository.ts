import { Comment } from '../model/entities/comment.entity';
import { Post } from '../model/entities/post.entity';
import { IdVO } from '../model/vos/id.vo';

export interface PostRepository {

  savePost(post: Post): Promise<void>;

  deletePost(postIdVO: IdVO): Promise<void>;

  updatePost(post: Post): Promise<void>;

  getPostById(postIdVO: IdVO): Promise<Post | null>;
  
  getAllPosts(): Promise<Post[]>;
  
  addComment(post: Post, comment: Comment): Promise<void>;
  
  deleteComment(commentIdVO: IdVO): Promise<void>;
  
  getCommentById(commentIdVO: IdVO): Promise<Comment | null>;

  updateComment(comment: Comment): Promise<void>;

}