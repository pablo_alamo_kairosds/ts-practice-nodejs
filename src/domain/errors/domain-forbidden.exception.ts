import { DomainException } from './domain.exception';

export class DomainForbiddenException extends DomainException {

  constructor(message = 'Not allowed') {
    super(message);
  }

}