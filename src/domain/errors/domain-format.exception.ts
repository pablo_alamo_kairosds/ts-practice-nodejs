import { DomainException } from './domain.exception';

export class DomainFormatException extends DomainException {

  constructor(message = 'Bad Request') {
    super(message);
  }

}