import { DomainException } from './domain.exception';

export class DomainConflictException extends DomainException {

  constructor(resourceName: string, resource: string) {
    super(`Invalid ${resourceName.toLowerCase()}. '${ resource }' already exists`);
  }

}