import { DomainException } from './domain.exception';

export class DomainNotFoundException extends DomainException {

  constructor(resource: string, field?: string) {
    super(`${resource} not found${field ? `: ${field}` : ''}`);
  }

}