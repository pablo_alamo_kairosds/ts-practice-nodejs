import { CustomError } from 'ts-custom-error';

export class DomainException extends CustomError { }