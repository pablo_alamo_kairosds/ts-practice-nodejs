import { v4 } from 'uuid';
import { IdVO } from './id.vo';

describe('ID VO', () => {

  it('should create', () => {
    const uuid: IdVO = IdVO.create();
    expect(uuid.value).not.toBeUndefined();
  });

  it('should create with uuid', () => {
    const myUUID: string = v4();
    const uuid: IdVO = IdVO.createWithUUID(myUUID);
    expect(uuid.value).toEqual(myUUID);
  });

  it('should throw error if invalid uuid id passed', () => {
    const invalidUUID = '32251827dabhvs678dsa';
    expect(() => IdVO.createWithUUID(invalidUUID)).toThrow(`Invalid ID: ${invalidUUID}. The ID is not a uuid`);
  });

});