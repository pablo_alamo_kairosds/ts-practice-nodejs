import { OffensiveWord } from '../entities/offensive-word.entity';
import { IdVO } from './id.vo';
import { LevelVO } from './level.vo';
import { MessageVO } from './message.vo';
import { WordVO } from './word.vo';

describe('Message VO', () => {

  it('shoud error when message is too long', () => {
    const message = 'dfhvsdhaffdfdsfdsfdsfnsdjlbfsdifgsbfsdjkfbdsshfoñdsbkb3j2wlb3frwjkldsadbjsag7d89asgoidusbkb3j2wlb3frwjkldsadbjsag7d89asgoiduslabnjdnsalñdnsakñdsnabjidlbsjkadbj23kbd3jk2lbd23kbdsabdjbfsjklfbsdjklfsdbjks';
    expect(() => MessageVO.create(message))
      .toThrow('The comment is too long');
  });

  it('shoud error when message is too short', () => {
    const message = '';
    expect(() => MessageVO.create(message))
      .toThrow('The comment is too short');
  });

  it('should create message with min characteres', () => {
    const message = 'x';
    const createdMessage: MessageVO = MessageVO.create(message);
    expect(createdMessage.value).toEqual(message);
  });

  it('should create message with max characteres', () => {
    const message = 'dfhvsdhaffdfdsfdsfdsfnsdjlbfsdifgsbfsdjkfbdsshfoñdsbkb3j2wlb3frwjkldsadbjsag7d89asgoidusbkb3j2wlb3frwjkldsadbjsag7d89asgoiduslabnjdnsalñdnsakñdsnabjidlbsjkadbj23kbd3jk2lbd23kbdsabdjbfsjklfbsdjklfsdbjk';
    const createdMessage: MessageVO = MessageVO.create(message);
    expect(createdMessage.value).toEqual(message);
  });
  
  it('should not create message because exists an offensive word', () => {
    const fakeOffensiveWords: OffensiveWord[] = [
      new OffensiveWord({ id: IdVO.create(), word: WordVO.create('fuck'), level: LevelVO.create(5) })
    ];

    const message = 'fuck';
    
    expect(() => MessageVO.create(message, fakeOffensiveWords))
      .toThrow('The comment contains 1 offensive words');
  });

});