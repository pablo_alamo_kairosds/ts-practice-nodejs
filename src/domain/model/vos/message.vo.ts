import { DomainFormatException } from '../../errors/domain-format.exception';
import { countOffensiveWords } from '../../services/count-offensive-words.service';
import { OffensiveWord } from '../entities/offensive-word.entity';

export class MessageVO {

  private static readonly MIN_LENGTH = 1;
  private static readonly MAX_LENGTH = 200;

  get value(): string {
    return this.message;
  }

  private constructor(private message: string) { }

  static create(message: string, offensiveWords: OffensiveWord[] = []): MessageVO {
    if (message.length < this.MIN_LENGTH) {
      throw new DomainFormatException('The comment is too short');
    }
    if (message.length > this.MAX_LENGTH) {
      throw new DomainFormatException('The comment is too long');
    }
    const offensiveWordsFound: number = countOffensiveWords(message, offensiveWords);
    if (offensiveWordsFound > 0) {
      throw new DomainFormatException(`The comment contains ${offensiveWordsFound} offensive words`);
    }

    return new MessageVO(message);
  }

}