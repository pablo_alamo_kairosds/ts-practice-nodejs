import { TitleVO } from './title.vo';

describe('Title VO', () => {

  it('should throw error when title is too short', () => {
    const title = '';
    expect(() => TitleVO.create(title)).toThrow('The title is too short');
  });

  it('should throw error when title is too long', () => {
    const title = 'abcfidhospfdñsfdfdjskfbsdjkbfsL';
    expect(() => TitleVO.create(title)).toThrow('The title is too long');
  });

  it('should create message with min characteres', () => {
    const title = 'a';
    const createdTitle: TitleVO = TitleVO.create(title);
    expect(createdTitle.value).toEqual(title);
  });

  it('should create message with max characteres', () => {
    const title = 'abcfidhospfdñsfdfdjskfbsdjkbfs';
    const createdTitle: TitleVO = TitleVO.create(title);
    expect(createdTitle.value).toEqual(title);
  });

});