import { PasswordVO } from './password.vo';

describe('Password VO', () => {

  it('should create', () => {
    const newPassword = 'pass';
    const password: PasswordVO = PasswordVO.create(newPassword);
    expect(password.value).toEqual(newPassword);
  }); 

});