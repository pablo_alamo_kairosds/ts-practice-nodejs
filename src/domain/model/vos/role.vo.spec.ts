import { Role, RoleVO } from './role.vo';

describe('Role VO', () => {

  it('should create', () => {
    const user: RoleVO = RoleVO.create(Role.USER);
    const admin: RoleVO = RoleVO.create(Role.ADMIN);
    const author: RoleVO = RoleVO.create(Role.AUTHOR);
    expect(user.value).toEqual(Role.USER);
    expect(admin.value).toEqual(Role.ADMIN);
    expect(author.value).toEqual(Role.AUTHOR);
  }); 

});