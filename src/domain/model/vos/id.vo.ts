import { v4, validate } from 'uuid';
import { DomainFormatException } from '../../errors/domain-format.exception';

export class IdVO {

  get value(): string {
    return this.id;
  }

  private constructor(private id: string) { }

  static create(): IdVO {
    return new IdVO(v4());
  }

  static createWithUUID(uuid: string): IdVO | never {
    if(!validate(uuid)) {
      throw new DomainFormatException(`Invalid ID: ${uuid}. The ID is not a uuid`);
    }
    return new IdVO(uuid);
  }
  
}