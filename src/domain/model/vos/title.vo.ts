import { DomainFormatException } from '../../errors/domain-format.exception';

export class TitleVO {
  
  private static readonly MIN_LENGTH = 1;
  private static readonly MAX_LENGTH = 30;

  get value(): string {
    return this.title;
  }

  private constructor(private title: string) { }

  static create(title: string): TitleVO {
    if (title.length < this.MIN_LENGTH) {
      throw new DomainFormatException('The title is too short');
    }
    if (title.length > this.MAX_LENGTH) {
      throw new DomainFormatException('The title is too long');
    }
    return new TitleVO(title);
  }
  
}