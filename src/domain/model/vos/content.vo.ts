import { DomainFormatException } from '../../errors/domain-format.exception';

export class ContentVO {

  private static readonly MIN_LENGTH = 50;
  private static readonly MAX_LENGTH = 300;

  get value(): string {
    return this.content;
  }

  private constructor(private content: string) { }

  static create(content: string): ContentVO {
    if (content.length < this.MIN_LENGTH) {
      throw new DomainFormatException('The content is too short');
    }
    if (content.length > this.MAX_LENGTH) {
      throw new DomainFormatException('The content is too long');
    }
    return new ContentVO(content);
  }

}