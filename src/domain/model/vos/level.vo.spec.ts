import { LevelVO } from './level.vo';

describe('Level VO', () => {

  it('should create', () => {
    const minLevel = 2;
    const level: LevelVO = LevelVO.create(minLevel);
    expect(level.value).toEqual(minLevel);
  });

  it('should throw error if level is greater', () => {
    const invalidLevel = 6;
    expect(() => LevelVO.create(invalidLevel)).toThrow(`Invalid level: ${invalidLevel}. Level must be between 1 and 5`);
  });

  it('should throw error if level is lower', () => {
    const invalidLevel = 0;
    expect(() => LevelVO.create(invalidLevel)).toThrow(`Invalid level: ${invalidLevel}. Level must be between 1 and 5`);
  });

});