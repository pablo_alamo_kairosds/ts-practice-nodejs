import { WordVO } from './word.vo';

describe('Word VO', () => {

  it('should create', () => {
    const newWord = 'Word';
    const word: WordVO = WordVO.create(newWord);
    expect(word.value).toEqual(newWord);
  }); 

});