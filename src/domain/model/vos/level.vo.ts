import { DomainFormatException } from '../../errors/domain-format.exception';

export class LevelVO {

  get value(): number {
    return this.level;
  }

  private constructor(private level: number) { }
  
  static create(level: number): never | LevelVO {
    if (level < 1 || level > 5) {
      throw new DomainFormatException(`Invalid level: ${level}. Level must be between 1 and 5`);
    }
    
    return new LevelVO(level);
  }
  
}