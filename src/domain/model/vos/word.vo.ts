export class WordVO {

  get value(): string {
    return this.word;
  }

  private constructor(private word: string) { }

  static create(level: string): WordVO {
    return new WordVO(level);
  }
  
}