import { ContentVO } from './content.vo';

describe('Content VO', () => {

  it('shoud error when content is too long', () => {
    const content = 'dfhvsdhaffdfdsfdsfdsfnsdjlbfsdifgsbfsdjkfbdsshfoñdsbkb3j2wlb3frwjkldsadbjsag7d89asgoidusbkb3j2wlb3frwjkldsadbjsag7d89asgoiduslabnjdnsalñdnsakñdsnabjidlbsjkadbj23kbd3jk2lbd23kbdsabdjbfsjklfbsdjklfsdbjkswjkldsadbjsag7d89asgoiduslabnjdnsalñdnsakñdsnabjidlbsjkadbj23kbd3jk2lbd23kbdsabdjbfsjklfbsdjklfsdbjk';
    expect(() => ContentVO.create(content))
      .toThrow('The content is too long');
  });

  it('shoud error when content is too short', () => {
    const content = 'dfhvsdhaffdfdsfdsfdsfnsdjlbfsdifgsbfsdjkfbdsshfoñ';
    expect(() => ContentVO.create(content))
      .toThrow('The content is too short');
  });

  it('should create content with min characteres', () => {
    const content = 'dfhvsdhaffdfdsfdsfdsfnsdjlbfsdifgsbfsdjkfbdsshfoñd';
    const createdContent: ContentVO = ContentVO.create(content);
    expect(createdContent.value).toEqual(content);
  });

  it('should create content with max characteres', () => {
    const content = 'dfhvsdhaffdfdsfdsfdsfnsdjlbfsdifgsbfsdjkfbdsshfoñdsbkb3j2wlb3frwjkldsadbjsag7d89asgoidusbkb3j2wlb3frwjkldsadbjsag7d89asgoiduslabnjdnsalñdnsakñdsnabjidlbsjkadbj23kbd3jk2lbd23kbdsabdjbfsjklfbsdjklfsdbjkswjkldsadbjsag7d89asgoiduslabnjdnsalñdnsakñdsnabjidlbsjkadbj23kbd3jk2lbd23kbdsabdjbfsjklfbsdjklfsdbj';
    const createdContent: ContentVO = ContentVO.create(content);
    expect(createdContent.value).toEqual(content);
  });

});