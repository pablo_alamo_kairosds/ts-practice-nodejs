import { EmailVO } from './email.vo';

describe('Email VO', () => {

  it('should create', () => {
    const newEmail = 'pass';
    const email: EmailVO = EmailVO.create(newEmail);
    expect(email.value).toEqual(newEmail);
  }); 

});