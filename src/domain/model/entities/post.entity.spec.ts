import { PostType, Post } from './post.entity';
import { IdVO } from '../vos/id.vo';
import { TitleVO } from '../vos/title.vo';
import { ContentVO } from '../vos/content.vo';
import { Comment, CommentType } from './comment.entity';
import { MessageVO } from '../vos/message.vo';

describe('Post Entity', () => {

  it('should create', () => {
    const commentType: CommentType = {
      id: IdVO.createWithUUID('7a4115f7-6e9c-4c15-b00d-f9b16f0b4f5c'),
      userId: IdVO.create(),
      message: MessageVO.create('Lorem ipsum')
    };
    const commentEntity = new Comment(commentType);

    const post: PostType = {
      id: IdVO.create(),
      userId: IdVO.create(),
      title: TitleVO.create('test'),
      content: ContentVO.create('Lorem ipsum dolor sit amet, consectetur adipiscing elit.'),
      comments: [commentEntity]
    };
    const postEntity = new Post(post);

    expect(postEntity.id).toEqual(post.id.value);
    expect(postEntity.userId).toEqual(post.userId.value);
    expect(postEntity.title).toEqual(post.title.value);
    expect(postEntity.content).toEqual(post.content.value);
  });

});