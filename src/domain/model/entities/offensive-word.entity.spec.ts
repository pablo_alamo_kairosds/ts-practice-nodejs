import { OffensiveWordType, OffensiveWord } from './offensive-word.entity';
import { IdVO } from '../vos/id.vo';
import { WordVO } from '../vos/word.vo';
import { LevelVO } from '../vos/level.vo';

describe('Offensive Word Entity', () => {

  it('should create', () => {
    const offensiveWord: OffensiveWordType = {
      id: IdVO.create(),
      word: WordVO.create('culo'),
      level: LevelVO.create(2)
    };
    const offensiveWordEntity = new OffensiveWord(offensiveWord);

    expect(offensiveWordEntity.id).toEqual(offensiveWord.id.value);
    expect(offensiveWordEntity.word).toEqual(offensiveWord.word.value);
    expect(offensiveWordEntity.level).toEqual(offensiveWord.level.value);

  });

});