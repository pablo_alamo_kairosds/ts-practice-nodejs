import { ContentVO } from '../vos/content.vo';
import { IdVO } from '../vos/id.vo';
import { TitleVO } from '../vos/title.vo';
import { Comment } from './comment.entity';

export type PostType = {
  id: IdVO,
  userId: IdVO,
  title: TitleVO,
  content: ContentVO,
  comments: Comment[];
}

export class Post {

  constructor(private post: PostType) {}

  get id(): string {
    return this.post.id.value;
  }

  get userId(): string {
    return this.post.userId.value;
  }

  get title(): string {
    return this.post.title.value;
  }

  get content(): string {
    return this.post.content.value;
  }

  get comments(): Comment[] {
    return this.post.comments;
  }
  
}