import { IdVO } from '../vos/id.vo';
import { MessageVO } from '../vos/message.vo';

export type CommentType = {
  id: IdVO,
  userId: IdVO,
  message: MessageVO
}

export class Comment {

  constructor(private comment: CommentType) {}

  get id(): string {
    return this.comment.id.value;
  }

  get userId(): string {
    return this.comment.userId.value;
  }

  get message(): string {
    return this.comment.message.value;
  }

}