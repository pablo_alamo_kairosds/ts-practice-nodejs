import { EmailVO } from '../vos/email.vo';
import { IdVO } from '../vos/id.vo';
import { PasswordVO } from '../vos/password.vo';
import { Role, RoleVO } from '../vos/role.vo';
import { User, UserType } from './user.entity';

describe('User Entity', () => {

  it('should create', () => {
    const user: UserType = {
      id: IdVO.create(),
      email: EmailVO.create('test@gmail.com'),
      password: PasswordVO.create('test'),
      role: RoleVO.create(Role.USER)
    };
    const userEntity = new User(user);

    expect(userEntity.id).toEqual(user.id.value);
    expect(userEntity.email).toEqual(user.email.value);
    expect(userEntity.password).toEqual(user.password.value);
    expect(userEntity.role).toEqual(user.role.value);

  });

});