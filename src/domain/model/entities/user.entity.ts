import { EmailVO } from '../vos/email.vo';
import { IdVO } from '../vos/id.vo';
import { PasswordVO } from '../vos/password.vo';
import { RoleVO } from '../vos/role.vo';

export type UserType = {
  id: IdVO,
  email: EmailVO,
  password: PasswordVO,
  role: RoleVO
}

export class User {

  constructor(private user: UserType) {}
  
  get id(): string {
    return this.user.id.value;
  }
  
  get email(): string {
    return this.user.email.value;
  }
  
  get password(): string {
    return this.user.password.value;
  }
  
  get role(): string {
    return this.user.role.value;
  }
  
}