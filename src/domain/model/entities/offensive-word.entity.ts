import { IdVO } from '../vos/id.vo';
import { LevelVO } from '../vos/level.vo';
import { WordVO } from '../vos/word.vo';

// Siempre añadir un id a la entidad de tipo string (más flexible), propio del dominio, diferente al de la db
export type OffensiveWordType = {
  id: IdVO;
  word: WordVO;
  level: LevelVO;
}

export class OffensiveWord {
  
  constructor(private offensiveWord: OffensiveWordType) {}

  // Se pretende hacer las clases lo más inmutable posible
  // Esto se traduce en solo declarar métodos get y no set (que modifican el objeto)
  
  get id(): string {
    return this.offensiveWord.id.value;
  }
  
  get word(): string {
    return this.offensiveWord.word.value;
  }

  get level(): number {
    return this.offensiveWord.level.value;
  }  
  
}