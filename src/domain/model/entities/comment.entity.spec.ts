import { CommentType, Comment } from './comment.entity';
import { IdVO } from '../vos/id.vo';
import { MessageVO } from '../vos/message.vo';

describe('Comment Entity', () => {

  it('should create', () => {
    const comment: CommentType = {
      id: IdVO.create(),
      userId: IdVO.create(),
      message: MessageVO.create('Lorem ipsum')
    };
    const commentEntity = new Comment(comment);

    expect(commentEntity.id).toEqual(comment.id.value);
    expect(commentEntity.userId).toEqual(comment.userId.value);
    expect(commentEntity.message).toEqual(comment.message.value);
  });

});